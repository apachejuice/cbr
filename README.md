# cbr
This is the developing branch. May not even compile.

## Setting up the project

**Via SSH (Recommended)**
```sh
git clone --recursive git@github.com:apachejuice/cbr
```

**Via HTTPS**
```sh
git clone --recursive https://www.github.com/apachejuice/cbr
````

## Prerequisuites

- libcheck
- meson

Note that libcheck *does* work on Windows platforms through msys (it needs pkg-config to function) but you need to make sure pkg-config can find libcheck (make sure it is in `/mingw64/bin/pkg-config`)

## Build Instructions

Enter each command in your terminal to compile the program.:
```sh
meson build
ninja -C build
```
NOTE: on msys targets, if you do not want to link with the msys dll, make sure to set the `CC` variable before running meson to your mingw64 c compiler.


Then you can run the program with:
```sh
./build/src/cbr
```

## So what about my Mac?
Good point! I recently got my hands on an old mac and have started development, but there are a lot of things to work on.
