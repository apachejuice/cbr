/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Debug main, for actual main see main.c
#include "bytecode/bcsource.h"
#include "bytecode/bctree.h"
#include "io/file.h"
#include "memory/alloc.h"
#include <stddef.h>
#include <stdio.h>

int main (int argc, char *argv[]) {
    u1 *data;
    size_t len;
    file f;

    file_open_simple (&f, argv[1], FM_READ);
    file_read_simple (&f, &data, &len);
    fprintf (stderr, "%zu\n", len);
    file_close (&f);

    bcsource src    = bcsource_new (data, len, argv[1]);
    struct bctree t = bc_parse (&src);
    bctree_dump (t);
    bcpool_destroy (t.pool);
    attrmap_free (t.glb_attrs);
    cfree (t.glb_attrs);
    cfree (data);
    return 0;
}
