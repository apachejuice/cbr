/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "vmerror.h"
#include "build-info.h"
#include "runtime/crash/trace.h"
#include "vmconfig.h"
#include "runtime/process.h"
#include "memory/alloc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Feature preprocessor macros.
#if ON_ASAN
#define ASAN_FEATURE "asan "
#else
#define ASAN_FEATURE ""
#endif

#if ON_THSAN
#define THSAN_FEATURE "thsan "
#else
#define THSAN_FEATURE ""
#endif

#if ON_UBSAN
#define UBSAN_FEATURE "ubsan "
#else
#define UBSAN_FEATURE ""
#endif

#if DEBUG
#define DEBUG_FEATURE "debug "
#else
#define DEBUG_FEATURE ""
#endif

#define FEATURES ASAN_FEATURE THSAN_FEATURE UBSAN_FEATURE DEBUG_FEATURE

// OS test macros.
#if OS_WINDOWS
#define OS_STR "Windows"
#elif OS_LINUX
#include <libgen.h>
#include <stdarg.h>
#define OS_STR "Linux"
#elif OS_DARWIN
#define OS_STR "Darwin/OS X"
#include <stdarg.h>
#else
#error Unknown os
#endif

static void err_print (cpid_t pid, const char *fmt, ...);
static void print_build_info (cpid_t pid, const vmbuildinfo bi);
static void print_run_info (cpid_t pid);
static void print_backtrace (cpid_t pid);
// used to normalize the result of __FILE__
static char *get_basename (char *path);

inline vmbuildinfo vmerror_get_buildinfo (void) {
    return (vmbuildinfo){
        .cc             = CBR_CC,
        .cflags         = CBR_BUILD_FLAGS,
        .cross_compiled = CBR_ISCROSS,
        .date           = CBR_BUILD_DATE,
        .time           = CBR_BUILD_TIME,
        .features       = FEATURES,
        .platform       = OS_STR,
    };
}

nothing vmerror_crash (const vmbuildinfo bi, const char *message,
                       const char *file, const char *func, size_t line) {
    char        *fname = cstrdup (file);
    char        *_file = get_basename (fname);
    const cpid_t pid   = process_pid_self ();
    err_print (pid, "The CBR experienced a problem and could not continue.");
    err_print (pid, "Please report this issue to " PACKAGE_BUGREPORT);
    err_print (pid, "");
    err_print (pid, "In file '%s' line %u (function '%s'): %s", _file, line,
               func, message);
    print_build_info (pid, bi);
    err_print (pid, "");
    print_run_info (pid);
    print_backtrace (pid);
    cfree (fname);
#if !OS_LINUX
    cfree (_file);
#endif
    process_exit (1);
}

nothing vmerror_assert_crash (const vmbuildinfo bi, const char *message,
                              const char *assertion, const char *file,
                              const char *func, size_t line) {
    size_t alen   = strlen (assertion);
    size_t msglen = strlen (message);
    // At this point a memory leak aint gonna hurt us.
    char *msg = ccalloc (alen + msglen + sizeof ("Assertion '' failed: ''"),
                         sizeof (char));
    sprintf (msg, "Assertion '%s' failed: %s", assertion, message);
    vmerror_crash (bi, msg, file, func, line);
}

void _vm_warning (const char *file, const char *func, size_t line,
                  const char *message, ...) {
    char *fname = cstrdup (file);
    char *_file = get_basename (fname);
    printf ("cbr: `%s()` (%s:%zu): WARNING: ", func, _file, line);
    cfree (fname);

    va_list ap;
    va_start (ap, message);
    vprintf (message, ap);
    va_end (ap);
    printf ("\n");
}

static void print_run_info (cpid_t pid) {
    err_print (pid, "VM process info: ");
    err_print (pid, "  PID: %u", pid);
    err_print (pid, "");
}

static void print_build_info (cpid_t pid, const vmbuildinfo bi) {
    err_print (pid, "VM build info: ");
    err_print (pid, "  C compiler:       %s", bi.cc);
    err_print (pid, "  C compiler flags: %s", bi.cflags);
    err_print (pid, "  Cross build:      %s", bi.cross_compiled ? "yes" : "no");
    err_print (pid, "  Build date:       %s", bi.date);
    err_print (pid, "  Build time:       %s", bi.time);
    err_print (pid, "  Build features:   %s",
               strspn (bi.features, " \r\n\t") == strlen (bi.features)
                   ? "<none>"
                   : bi.features);
    err_print (pid, "  Build platform:   %s", bi.platform);
}

static void err_print (cpid_t pid, const char *fmt, ...) {
    fprintf (stderr, "#! " CPID_FMT ": ", pidf (pid));
    va_list ap;
    va_start (ap, fmt);
    vfprintf (stderr, fmt, ap);
    fprintf (stderr, "\n");
    va_end (ap);
}

static void print_backtrace (cpid_t pid) {
    int    n;
    char **trace = dbg_trace (15, &n);
    err_print (pid, "Calling thread stack trace:");

    for (int i = 0; i < n; i++) {
        err_print (pid, "    -> %s", get_basename (trace[i]));
    }

    cfree (trace);
}

static char *get_basename (char *file) {
#if OS_LINUX
    return basename (file);
#else
    char *p = strrchr (file, '/');
    return p ? p + 1 : file;
#endif
}
