/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_wtypes_h__
#define __cbr_wtypes_h__
#include <stdint.h>

typedef unsigned char u1;
typedef signed char   s1;

typedef uint16_t u2;
typedef int16_t  s2;

typedef uint32_t u3, u4;
typedef int32_t  s3, s4;

typedef uint64_t u8;
typedef int64_t  s8;

#endif
