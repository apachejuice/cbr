/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_tls_h__
#define __cbr_tls_h__
#include "platform-types.h"

/* Represents a thread-local storage type. */
typedef _CBR_THLOCALTYPE tls;

/* Create a new thread-local storage slot. */
tls tls_new ();

/* Create a new thread-local storage slot with data `d`. */
tls tls_new_data (void *d);

/* Sets the data of slot `s` to `d`. */
void tls_set_data (tls *s, void *d);

/* Returns the data of slot `s`. If the slot is illegal, `err` is set to `true`. */
void *tls_get_data (tls *s, bool *err);

/* Free slot `s`. */
void tls_free (tls *s);

#endif
