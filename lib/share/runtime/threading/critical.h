/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_critical_h__
#define __cbr_critical_h__

#include "platform-types.h"

/* Represents a critical section. It is for linux the same as a mutex. For NT,
it is about 30 times faster than a mutex, but can only be used in the same process. */
typedef _CBR_CRITICALTYPE critical;

/* Creates a new critical section. */
critical critical_new ();

/* Enters the critical section. */
void critical_enter (critical *critical);

/* Exits the critical section. */
void critical_exit (critical *critical);

/* Destroys the critical section. */
void critical_destroy (critical *critical);

#endif
