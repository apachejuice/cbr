/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CBR_THREADING_INTERNAL
#error Do not include this header from outside the 'threading' folder
#endif

#ifndef __cbr_thread_internal_h__
#define __cbr_thread_internal_h__
#include "thread.h"
#define TH_STACK_INIT 10

#define th_set_flag(th, f)      ((th)->_flags |= (f))
#define th_override_flag(th, f) ((th)->_flags = (f))

/* vmThread internal initialization arguments. */
typedef struct {
    threadBase        *t;
    bundle            *options;
    vmThreadPreExecCB  pre_cb;
    vmThreadPostExecCB post_cb;
    vmThreadMainFunc   main;
} vmThreadInitArgs;

struct vmThread {
    /* Base pointer. KEEP THIS AS THE FIRST MEMBER. */
    threadBase *_p;
    /* The pre-execution function of this thread. */
    vmThreadPreExecCB pre_cb;
    /* The post-execution function of this thread. */
    vmThreadPostExecCB post_cb;
    /* The main loop of this thread. */
    vmThreadMainFunc main;
};

enum {
    THF_NEW    = 1 << 0,
    THF_PREXC  = 1 << 1,
    THF_POSTXC = 1 << 2,
    THF_MAIN   = 1 << 3,
    THF_DEAD   = 1 << 4,
};

static threadBase  *main_thread_info;
static threadBase **th_stack;

// clang-format off
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
static void _default_preexec (const threadBase *self) {}
static void _default_postexec (const threadBase *self) {}
#pragma GCC diagnostic pop
// clang-format on

// Hack to suppress clangd errors under msys
// TODO: Relies on thread_local being a macro, is it guaranteed to be?
#ifndef thread_local
#define thread_local
#endif

#endif
