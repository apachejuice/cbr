/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_barrier_h__
#define __cbr_barrier_h__
#include "platform-types.h"

/* Represents a thread barrier. */
typedef _CBR_BARRIERTYPE barrier;

/* Creates a new barrier. This barrier trips when `nthreads` threads have arrived in it.

On windowns, threads will spin `nspin` times when they block on the barrier, and after it is exceeded they block passively.
If `nspin` is 0, threads block passively when they start waiting on the barrier.

On linux, threads will block immediately. */
barrier barrier_new (unsigned nthreads, unsigned nspin);

/* Wait on the barrier. Returns `true` for the last thread that enters the barrier, `false` for all others. */
bool barrier_wait (barrier *barr);

/* Destroys the memory used by the barrier. */
void barrier_destroy (barrier *barr);

#endif
