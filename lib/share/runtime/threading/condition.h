/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_condition_h__
#define __cbr_condition_h__
#include <stdbool.h>
#include "platform-types.h"
#include "runtime/threading/critical.h"

/* Represents a condition variable. */
typedef _CBR_CONDTYPE condition;

/* Create a new condition variable. */
condition condition_new ();

/* Sleep on the condition variable forever and unlock `crit`. If `crit` is not entered, fail. 
On success, the return value is `true`, else `false`. */
bool condition_sleep_forever (condition *cond, critical *crit);

/* Sleep on the condition variable for `n` milliseconds and unlock `crit`. If `crit` is not entered, fail. 
On success, the return value is `true`, else `false`. */
bool condition_sleep (condition *cond, critical *crit, millis_t n);

/* Wakes a single thread waiting on the condition variable. */
void condition_wake (condition *cond);

/* Wakes all threads on the condition variable. */
void condition_wake_all (condition *cond);

/* Destroys the condition variable. */
void condition_destroy (condition *cond);

#endif
