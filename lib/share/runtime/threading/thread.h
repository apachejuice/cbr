/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_thread_h__
#define __cbr_thread_h__
#include "collections/bundle.h"
#include "macro.h"
#include "platform-types.h"
#include <stdint.h>
#include <limits.h>

#define THID_MAX     USHRT_MAX
#define TH_MAIN_NAME "os_main"
#define TH_FMT       _CBR_THREADFMT

/* *th_run return status */
enum {
    /* Dead or already running */
    TH_STATUS_AGAIN = -1,
    /* Success */
    TH_STATUS_SUCCESS = 0,
    /* All other statuses indicate error.
    TODO: map them into os counterparts? */
};

/* Initialize the threading system. This method MUST be called from the main thread.
In order to use the CBR threading system, this function must be called once. Subsequent calls are ignored. */
void th_init ();

/* Deinitialize the threading system, dropping any resources associated with it.
After a call to this no new threads can be allocated, although old ones can continue. */
void th_deinit ();

/* Exits the calling thread with the value `ptr`. */
nothing th_exit (void *ptr);

/* Pause the current thread for `n` milliseconds. */
void th_sleep (millis_t n);

/* Defines a thread base type that all threads have as a member. */
typedef struct threadBase threadBase;
/* Represents the ID of a thread as an integer. This is a successive integer by thread count. 0 is reserved for the original C thread. */
typedef unsigned short thid_t;
/* Represents the native id of a thread as an integer. This may be whatever the OS has set. */
typedef _CBR_THREADID nthid_t;
/* Represents a pre-execution function for a thread. */
typedef void (*vmThreadPreExecCB) (const threadBase *self);
/* Represents a post-execution function for a thread. */
typedef void (*vmThreadPostExecCB) (const threadBase *self);
/* Represents a thread main loop. */
typedef void (*vmThreadMainFunc) (const threadBase *self, bundle *options);
/* Represents the state of a thread. */
typedef enum {
    /* The thread has not been started yet. */
    TS_NEWBORN,
    /* The thread is running its pre-execution hooks. */
    TS_PREEXEC,
    /* The thread is running its post-execution hoooks. */
    TS_POSTEXEC,
    /* The thread is running its main loop. */
    TS_MAIN,
    /* The thread is dead. */
    TS_DEAD,
} threadState;

struct threadBase {
    /* The VM id of the thread. */
    thid_t vm_id;
    /* The OS id of the thread. */
    nthid_t os_id;
    /* The thread's name. */
    const char *name;
    /* The thread that started this thread. Always a valid thread. */
    const threadBase *parent;
    /* Private flags. Do not modify. */
    unsigned int _flags;
    /* Private data. Do not modify. */
    void *_data;
};

/* Gets the state of a thread. */
threadState th_get_state (const threadBase *th);
/* Get the current thread. */
threadBase *th_self (void);
/* Join the thread. Returns the amount of milliseconds between calling this function and its return. If data is not null, thread return value is stored in data. */
double th_join (const threadBase *th, void **data);

/* Represents a native code thread that runs C code. */
typedef struct vmThread vmThread;

/* Create a new vmThread, but do not launch it. */
vmThread *vmt_new (const char *name, vmThreadPreExecCB pre_cb,
                   vmThreadPostExecCB post_cb, notnull vmThreadMainFunc main,
                   const threadBase *parent, thid_t *id);
/* Create a new vmThread with some default values. */
static inline vmThread *vmt_new_default (const char      *name,
                                         vmThreadMainFunc main) {
    return vmt_new (name, NULL, NULL, main, th_self (), NULL);
}

/* Cast into a threadBase*. */
threadBase *vmt_base (const vmThread *th);

/* Run the thread. */
int vmt_run (vmThread *th, bundle *arg);

/* Free the resources held by the thread. Does NOT free the argument bundle, but the internal constructs the thread used to pass data. */
void vmt_free (vmThread *th);

#endif /* __cbr_thread_h__ */
