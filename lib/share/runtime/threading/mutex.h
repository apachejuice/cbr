/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_mutex_h__
#define __cbr_mutex_h__
#include <stdbool.h>
#include "platform-types.h"

/* Represents a mutex. */
typedef _CBR_MUTEXTYPE mutex;

/* Creates a new mutex. */
mutex *mutex_new ();

/* Destroys the memory held by mtx. If mtx is owned by a thread, the program crashes. */
void mutex_destroy (mutex *mtx);

/* Lock mtx. If it is already locked, return false. Else return true. */
bool mutex_lock (mutex *mtx);

/* Unlock mtx. If it is not locked, do nothing. */
void mutex_unlock (mutex *mtx);

#endif
