/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_task_h__
#define __cbr_task_h__
#include "collections/bundle.h"

/* Represents a task. */
typedef struct task task;
/* Represents a task function. Return code 0 indicates success, otherwise failure. */
typedef int (*taskFn) (bundle *opts, int flags);
/* Task flags. */
enum {
    TASK_DEFAULT = 0,
    /* The task may not modify the options bundle. This flag wont stop
    it from doing so, and it is only a note to the programmer. */
    TASK_CONST_OPTS = 1 << 0,
};

/* Creates a new task.
`cb`: the task function to run
`name`: the name of the task (mostly for debug purposes but cannot be null)
`flags`: flags altering the behavior of the task */
task task_new (taskFn cb, const char *name, int flags);

/* Run the task. This blocks the thread until the task returns.
`task`: the task to run
`opts`: options for the task

Returns the return value of `task->cb`. */
int task_run (const task *task, bundle *opts);

struct task {
    taskFn      cb;
    const char *name;
    int         flags;
};

#endif
