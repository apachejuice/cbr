/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License"){}
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "order.h"

#if (defined(__GNUC__) && !defined(__ICC)) || defined(__llvm__) || \
    defined(__clang__)
#define s16swap(val) __builtin_bswap16 (val)
#define u16swap(val) __builtin_bswap16 (val)
#define s32swap(val) __builtin_bswap32 (val)
#define u32swap(val) __builtin_bswap32 (val)
#define s64swap(val) __builtin_bswap64 (val)
#define u64swap(val) __builtin_bswap64 (val)
#else
#error TODO byte swapping for non-gcc compilers
#endif

inline s2 short_swap (s2 src) {
    return s16swap (src);
}

inline u2 ushort_swap (u2 src) {
    return u16swap (src);
}

inline s4 int_swap (s4 src) {
    return s32swap (src);
}

inline u4 uint_swap (u4 src) {
    return u32swap (src);
}

inline s8 long_swap (s8 src) {
    return s64swap (src);
}

inline u8 ulong_swap (u8 src) {
    return u64swap (src);
}
