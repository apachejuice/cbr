/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_process_h__
#define __cbr_process_h__
#include "platform-types.h"
#include "macro.h"

/* Represents the process ID. */
typedef _CBR_PIDTYPE cpid_t;

/* Returns the PID for the current process. */
cpid_t process_pid_self (void);

/* Exits the current process with code c; */
nothing process_exit (unsigned c);

#endif
