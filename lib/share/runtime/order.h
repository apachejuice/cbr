/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_order_h__
#define __cbr_order_h__
#include "vmconfig.h"
#include "wtypes.h"

/* Byte order conversions. The idea here is that you rarely need to expose yourself to big-endian
and little-endian outside of this file; the API treats them as CBR integers and native integers. */
#if CBR_BIGENDIAN_HOST
#define native_short_to_cbr(x)  (x)
#define native_ushort_to_cbr(x) (x)
#define native_int_to_cbr(x)    (x)
#define native_uint_to_cbr(x)   (x)
#define native_long_to_cbr(x)   (x)
#define native_ulong_to_cbr(x)  (x)
#else
#define native_short_to_cbr(x)  short_swap (x)
#define native_ushort_to_cbr(x) ushort_swap (x)
#define native_int_to_cbr(x)    int_swap (x)
#define native_uint_to_cbr(x)   uint_swap (x)
#define native_long_to_cbr(x)   long_swap (x)
#define native_ulong_to_cbr(x)  ulong_swap (x)
#endif

s2 short_swap (s2 src);
u2 ushort_swap (u2 src);

s4 int_swap (s4 src);
u4 uint_swap (u4 src);

s8 long_swap (s8 src);
u8 ulong_swap (u8 src);

#endif
