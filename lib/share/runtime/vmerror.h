/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_vmerror_h__
#define __cbr_vmerror_h__
#include <stdbool.h>
#include <stddef.h>
#include "macro.h"

/* Carries information about the VM build. */
typedef struct {
    /* The build date in DD-MM-YYYY. */
    const char *date;
    /* The time of running configure in HH:MM:SS. */
    const char *time;
    /* The c compiler flags used to build. */
    const char *cflags;
    /* The OS name as detected by AC_CANONICAL_TARGET. */
    const char *platform;
    /* The features/debug info the VM was built with. */
    const char *features;
    /* The C compiler used. */
    const char *cc;
    /* Was this build cross-compiled? */
    bool cross_compiled;
} vmbuildinfo;

/* Returns the build info of this VM. */
vmbuildinfo vmerror_get_buildinfo (void);

/* Reports an error and ends the VM. */
nothing vmerror_crash (const vmbuildinfo bi, const char *message,
                       const char *file, const char *func, size_t line);

/* Reports an assertion error and ends the VM. */
nothing vmerror_assert_crash (const vmbuildinfo bi, const char *message,
                              const char *assertion, const char *file,
                              const char *func, size_t line);

/* Reports a warning message to stdout. */
void _vm_warning (const char *file, const char *func, size_t line,
                  const char *message, ...);

/* Asserts `x` with message `m`. */
#define vm_assert(x, m)                                                      \
    do {                                                                     \
        if (!(x)) {                                                          \
            vmerror_assert_crash (vmerror_get_buildinfo (), m, #x, __FILE__, \
                                  __func__, __LINE__);                       \
        }                                                                    \
    } while (0)

/* For debug builds */
#define vm_assertd(x, m) vm_assert (x, "DEBUG: " m)

/* Asserts `x`. */
#define _vm_assert(x) vm_assert (x, #x)

/* Formatted warning message to stdout. */
#define vm_warningf(msg, ...) \
    _vm_warning (__FILE__, __func__, __LINE__, msg, __VA_ARGS__)

#define vm_warning(msg) _vm_warning (__FILE__, __func__, __LINE__, msg);

#endif
