/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_bundle_h__
#define __cbr_bundle_h__
#include <stddef.h>
#include <stdbool.h>

/* A hash function for a bundle.  */
typedef size_t (*bundleHashFunc) (const char *key, size_t nb, size_t att);

/* Represents a typed hash table that maps string keys to different values. */
typedef struct bundle bundle;

/* Creates a new bundle. */
bundle *bundle_create (void);
/* Creates a new bundle with the specified hash function. */
bundle *bundle_create_hf (bundleHashFunc hf);
/* Gets the amount of elements in the bundle. */
size_t bundle_size (const bundle *b);
/* Add all the keys not in `dst` from `src` to `dst`. */
size_t bundle_merge (bundle *dst, const bundle *src, size_t *nexist);
/* Does the key exist? */
bool bundle_has_key (const bundle *b, const char *key);
/* Free the memory used by bundle. */
void bundle_destroy (bundle *b);

/* Adds an integer to the bundle with the specified key. If the key is NULL or it already exists in the bundle, nothing is done. */
void bundle_put_int (bundle *b, const char *key, int v);
/* Adds a long to the bundle with the specified key. If the key is NULL or it already exists in the bundle, nothing is done. */
void bundle_put_long (bundle *b, const char *key, long v);
/* Adds a long long to the bundle with the specified key. If the key is NULL or it already exists in the bundle, nothing is done. */
void bundle_put_long_long (bundle *b, const char *key, long long v);
/* Adds a float to the bundle with the specified key. If the key is NULL or it already exists in the bundle, nothing is done. */
void bundle_put_float (bundle *b, const char *key, float v);
/* Adds a double to the bundle with the specified key. If the key is NULL or it already exists in the bundle, nothing is done. */
void bundle_put_double (bundle *b, const char *key, double v);
/* Adds a string to the bundle with the specified key. If the key is NULL or it already exists in the bundle, nothing is done. */
void bundle_put_str (bundle *b, const char *key, char *v);
/* Adds a void pointer to the bundle with the specified key. If the key is NULL or it already exists in the bundle, nothing is done. */
void bundle_put_voidp (bundle *b, const char *key, void *v);
/* Adds a bundle to the bundle with the specified key. If the key is NULL or it already exists in the bundle, nothing is done. */
void bundle_put_bundle (bundle *b, const char *key, bundle *v);

/* Gets an integer from the bundle with the specified key.If the key does not exist in the bundle,
`ptr` is unchanged and a nonzero value is returned. Otherwise, 0 is returned and `ptr` is set to the corresponding value. */
int bundle_get_int (const bundle *b, const char *key, int *ptr);
/* Gets a long from the bundle with the specified key.If the key does not exist in the bundle,
`ptr` is unchanged and a nonzero value is returned. Otherwise, 0 is returned and `ptr` is set to the corresponding value. */
int bundle_get_long (const bundle *b, const char *key, long *ptr);
/* Gets a long long from the bundle with the specified key.If the key does not exist in the bundle,
`ptr` is unchanged and a nonzero value is returned. Otherwise, 0 is returned and `ptr` is set to the corresponding value. */
int bundle_get_long_long (const bundle *b, const char *key, long long *ptr);
/* Gets a float from the bundle with the specified key.If the key does not exist in the bundle,
`ptr` is unchanged and a nonzero value is returned. Otherwise, 0 is returned and `ptr` is set to the corresponding value. */
int bundle_get_float (const bundle *b, const char *key, float *ptr);
/* Gets a double from the bundle with the specified key.If the key does not exist in the bundle,
`ptr` is unchanged and a nonzero value is returned. Otherwise, 0 is returned and `ptr` is set to the corresponding value. */
int bundle_get_double (const bundle *b, const char *key, double *ptr);
/* Gets a string from the bundle with the specified key.If the key does not exist in the bundle,
`ptr` is unchanged and a nonzero value is returned. Otherwise, 0 is returned and `ptr` is set to the corresponding value. */
int bundle_get_str (const bundle *b, const char *key, char **ptr);
/* Gets a void pointer from the bundle with the specified key.If the key does not exist in the bundle,
`ptr` is unchanged and a nonzero value is returned. Otherwise, 0 is returned and `ptr` is set to the corresponding value. */
int bundle_get_voidp (const bundle *b, const char *key, void **ptr);
/* Gets a bundle from the bundle with the specified key.If the key does not exist in the bundle,
`ptr` is unchanged and a nonzero value is returned. Otherwise, 0 is returned and `ptr` is set to the corresponding value. */
int bundle_get_bundle (const bundle *b, const char *key, bundle **ptr);

#endif
