/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "bundle.h"
#include "macro.h"
#include <stdio.h>
#include "memory/alloc.h"
#include <string.h>

extern char *strdup (const char *src);

/* Represents the element type of a bundle. */
typedef enum {
    BET_INT,
    BET_LONG,
    BET_LONG_LONG,
    BET_FLOAT,
    BET_DOUBLE,
    BET_STRING,
    BET_VOIDP,
    BET_BUNDLE,
} bundleElemType;

static size_t keyhash (const char *s, size_t p, size_t nb) {
    size_t       hash  = 2166136261u;
    const size_t len_s = strlen (s);
    for (size_t i = 0; i < len_s; i++) {
        hash ^= s[i];
        hash *= 16777619;
    }

    return hash % nb;
}

/* Represents an element in the bundle. */
typedef struct {
    bundleElemType type;
    char          *key;
    union {
        int       i;
        long      l;
        long long ll;
        float     f;
        double    d;
        char     *s;
        void     *v;
        bundle   *b;
    };
} bundleElem;

typedef struct bundle {
    size_t         cur_size;
    size_t         cap;
    bundleElem   **top;
    bundleHashFunc hf;
} bundle;

static inline bundleElem *elem_create (bundleElemType type, char *key) {
    bundleElem *e = cmalloc (sizeof (bundleElem));
    e->type       = type;
    e->key        = key;
    return e;
}

#define elem_del(elem)     \
    do {                   \
        cfree (elem->key); \
        cfree (elem);      \
    } while (0)

#define bundle_init_sz 10
#define insane_free(ptr) \
    do {                 \
        cfree (ptr);     \
        ptr = NULL;      \
    } while (0)

bundle *bundle_create () {
    return bundle_create_hf (&keyhash);
}

bundle *bundle_create_hf (bundleHashFunc hf) {
    bundle *bundle   = cmalloc (sizeof (struct bundle));
    bundle->cap      = bundle_init_sz;
    bundle->cur_size = 0;
    bundle->hf       = hf;
    bundle->top      = ccalloc (sizeof (bundleElem *), bundle_init_sz);
    return bundle;
}

inline size_t bundle_size (const bundle *bundle) {
    return bundle->cur_size;
}

bool bundle_has_key (const bundle *bundle, const char *key) {
    for (size_t i = 0; i < bundle_size (bundle); i++) {
        if (strcmp (bundle->top[i]->key, key) == 0) {
            return true;
        }
    }

    return false;
}

size_t bundle_merge (bundle *dst, const bundle *src, size_t *nexist) {
    size_t i;
    size_t ec = 0;

    for (i = 0; i < bundle_size (src); i++) {
        bundleElem e = *src->top[i];  // maybe cheaper/safer to dereference?
        if (!bundle_has_key (dst, e.key)) {
            bundleElemType t   = e.type;
            const char    *key = e.key;

            switch (t) {
                case BET_INT: bundle_put_int (dst, key, e.i); break;
                case BET_LONG: bundle_put_long (dst, key, e.l); break;
                case BET_LONG_LONG:
                    bundle_put_long_long (dst, key, e.ll);
                    break;
                case BET_FLOAT: bundle_put_float (dst, key, e.f); break;
                case BET_DOUBLE: bundle_put_double (dst, key, e.d); break;
                case BET_STRING: bundle_put_str (dst, key, e.s); break;
                case BET_VOIDP: bundle_put_voidp (dst, key, e.v); break;
                case BET_BUNDLE: bundle_put_bundle (dst, key, e.b); break;
            }
        } else {
            ec++;
        }
    }

    if (nexist != NULL) {
        *nexist = ec;
    }

    return i;
}

void bundle_destroy (bundle *bundle) {
    for (size_t i = 0; i < bundle->cur_size; i++) {
        elem_del (bundle->top[i]);
    }

    insane_free (bundle->top);
    bundle->cur_size = 0;
    bundle->cap      = 0;
    cfree (bundle);
}

// A function template for put functions
#define BNDL_PUT_FNDEF(np, t, bet, m)                                     \
    void bundle_put_##np (bundle *bundle, const char *key, t v) {         \
        if (bundle_has_key (bundle, key)) {                               \
            return;                                                       \
        }                                                                 \
                                                                          \
        if (bundle->top[bundle->cap - 1] != NULL) {                       \
            bundle->top = crealloc (                                      \
                bundle->top, sizeof (bundleElem *) * (bundle->cap *= 2)); \
        }                                                                 \
                                                                          \
        for (size_t i = 0; i < bundle->cap; i++) {                        \
            if (bundle->top[i] == NULL) {                                 \
                bundle->top[i]    = elem_create (bet, cstrdup (key));     \
                bundle->top[i]->m = v;                                    \
                break;                                                    \
            }                                                             \
        }                                                                 \
                                                                          \
        bundle->cur_size++;                                               \
    }

#define _bll     long long
#define _bstr    char *
#define _bvoidp  void *
#define _bbundle bundle *

BNDL_PUT_FNDEF (int, int, BET_INT, i)
BNDL_PUT_FNDEF (long, long, BET_LONG, l)
BNDL_PUT_FNDEF (long_long, _bll, BET_LONG_LONG, ll)
BNDL_PUT_FNDEF (float, float, BET_FLOAT, f)
BNDL_PUT_FNDEF (double, double, BET_DOUBLE, d)
BNDL_PUT_FNDEF (str, _bstr, BET_STRING, s)
BNDL_PUT_FNDEF (voidp, _bvoidp, BET_VOIDP, v)
BNDL_PUT_FNDEF (bundle, struct _bbundle, BET_BUNDLE, b)

#define BNDL_GET_FNDEF(np, t, m, bet)                                       \
    int bundle_get_##np (const bundle *bundle, const char *key, t *value) { \
        size_t      index = bundle->hf (key, bundle_size (bundle), 1);      \
        bundleElem *item  = bundle->top[index];                             \
        size_t      i     = 1;                                              \
                                                                            \
        while (item != NULL) {                                              \
            if (strcmp (item->key, key) == 0) {                             \
                if (item->type != bet) {                                    \
                    return 1;                                               \
                }                                                           \
                                                                            \
                *value = item->m;                                           \
                return 0;                                                   \
            }                                                               \
                                                                            \
            index = bundle->hf (key, bundle_size (bundle), i);              \
            item  = bundle->top[index];                                     \
            i++;                                                            \
        }                                                                   \
                                                                            \
        return 1;                                                           \
    }

BNDL_GET_FNDEF (int, int, i, BET_INT)
BNDL_GET_FNDEF (long, long, l, BET_LONG)
BNDL_GET_FNDEF (long_long, _bll, ll, BET_LONG_LONG)
BNDL_GET_FNDEF (float, float, f, BET_FLOAT)
BNDL_GET_FNDEF (double, double, d, BET_DOUBLE)
BNDL_GET_FNDEF (str, _bstr, s, BET_STRING)
BNDL_GET_FNDEF (voidp, _bvoidp, v, BET_VOIDP)
BNDL_GET_FNDEF (bundle, struct _bbundle, b, BET_BUNDLE)
