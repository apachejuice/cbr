/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "file.h"

bool file_mode_check (int mode) {
    // Must have one of: read, write, append
    if (!(mode & (FM_READ | FM_WRITE | FM_APPEND))) {
        return false;
    } else {
        if (mode & FM_READ) {
            return !(mode & (FM_WRITE | FM_APPEND));
        } else if (mode & FM_WRITE) {
            return !(mode & (FM_READ | FM_APPEND));
        } else {
            return !(mode & (FM_WRITE | FM_READ));
        }
    }
}
