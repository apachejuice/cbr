/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_file_h__
#define __cbr_file_h__
#include "runtime/wtypes.h"
#include "platform-types.h"
#include "collections/bundle.h"
#include <stddef.h>
#include <stdbool.h>
#include <string.h>

/* Represents a file error. */
typedef enum {
    /* No error. */
    F_ERR_SUCCESS = 0,
    /* File not found. */
    F_ERR_NOT_FOUND,
    /* File cannot be read. */
    F_ERR_NOREAD,
    /* File is too large to be read. */
    F_ERR_2BIG,
    /* File is a directory. */
    F_ERR_ISDIR,
    /* Unknown error. */
    F_ERR_NOKNOW,
} fileError;

#define F_ERR_SUCCESS(e) (e == F_ERR_SUCCESS)
#define F_ERR_FAILURE(e) (e != F_ERR_SUCCESS)
#define F_ERR_KNOWN(e)   (e != F_ERR_NOKNOW)

/* Read `file` as a byte array and return the contents. If `txt`, add `\0` to the end of the created array.
Sets `len` to the length of the file and `err` to an error code in `fileError`.
The returned array may be freed with `cfree`. */
u1 *read_file (const char *file, size_t *len, bool txt, fileError *err);

/* Represents a file handle. */
typedef struct {
    int                 mode;
    _CBR_FILEHANDLETYPE hndl;
} file;
/* Initial value for a file handle. */
#if OS_WINDOWS
#define FILEHANDLE_INIT ((file){-1, INVALID_HANDLE_VALUE})
#else
#define FILEHANDLE_INIT ((file){-1, -1})
#endif
#define NO_FILE_HANDLE FILEHANDLE_INIT

enum {
    // Mode for reading the file.
    FM_READ = 1 << 0,
    // Mode for creating the file, if it doesn't exist.
    FM_CREATE = 1 << 1,
    // Mode for writing the file. Note that using this mode empties the file.
    FM_WRITE = 1 << 2,
    // Mode for appending to the file
    FM_APPEND = 1 << 3,
    // Don't assume the file's contents are valid in any encoding.
    FM_BINARY = 1 << 4,

    // A basic read/create mode.
    FM_RPLUS = FM_READ | FM_CREATE,
    // A basic write/create mode.
    FM_WPLUS = FM_WRITE | FM_CREATE,
    // A basic append/create mode.
    FM_APLUS = FM_APPEND | FM_CREATE,
    // Appending to a binary file.
    FM_BAPP = FM_BINARY | FM_APPEND,
};

/* Use this to create a new file handle which doesnt point anywhere. */
#define file_new_lone() (FILEHANDLE_INIT)

/* Returns true if the given file handle is not pointing to a file. */
#define file_is_lone(fhandle) (fhandle == NO_FILE_HANDLE)

/* Opens the specified file handle to the relative or absolute path `path`.
Whether the path is relative or absolute is determined by the first character of `path`.
`mode` contains flags for opening the file; see the `FM_*` macros.
Returns an error code on error. */
int file_open_simple (file *handle, const char *path, int mode);

/* Reads the contents of the file. Returns an error code or 0. */
int file_read_simple (file *handle, u1 **buf, size_t *size);

/* Writes len bytes of data to handle. Returns an error code or 0. */
int file_write_simple (file *handle, const u1 *data, size_t len);

/* Writes the string s to handle. */
#define file_write_text(handle, s) \
    (file_write_simple (handle, (const u1 *) s, strlen (s)))

/* Closes the file handle. It can be reused again by opening it. */
int file_close (file *handle);

/* Is the specified file mode valid? */
bool file_mode_check (int mode);

#endif
