/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_console_h__
#define __cbr_console_h__
/* This file implements only color changes currently.
The goal is to implement a whole API. */

#include <stdbool.h>
#include <stdarg.h>
#include "platform-types.h"

/* Is c a valid rgb color value? (0-255) */
#define IS_COLOR(c) (c >= 0 && c <= 255)

/* Represents a console object. */
typedef _CBR_CONSOLETYPE console;

/* Creates a new console object to stdout. */
console console_stdout (void);
/* Creates a new console object to stderr. */
console console_stderr (void);

/* Write n bytes of buf to the console. Returns the amount of bytes written. */
size_t console_lwrite (console *con, const void *buf, size_t n);
/* Write str to the console. Returns the amount of bytes written. */
size_t console_write (console *con, const char *str);
/* Write a formatted string to the console. */
size_t console_printf (console *con, const char *str, ...);
/* Write a formatter string to the console using a va_list. */
size_t console_vprintf (console *con, const char *str, va_list ap);

/* Sets the console text color. */
bool console_set_text_color (console *con, int red, int green, int blue);
/* Resets the console text color to its original value. */
void console_reset_text_color (console *con);

#endif
