/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "alloc.h"
#include "write.h"
#include "runtime/vmerror.h"
#include "vmconfig.h"
#include <stdlib.h>
#include <string.h>

void *cmalloc (size_t n) {
    vm_assert (n != 0, "cmalloc(): illegal allocation");
    void *ptr = malloc (n);
    vm_assert (ptr != NULL, "`malloc` failed");
    return ptr;
}

void *ccalloc (size_t n, size_t x) {
    vm_assert (n != 0 && x != 0, "ccalloc(): illegal allocation");
    void *ptr = calloc (n, x);
    vm_assert (ptr != NULL, "`calloc` failed");
    return ptr;
}

void *crealloc (void *x, size_t n) {
    // Normal realloc tolerates NULL pointers. We do not.
    vm_assert (x != NULL && n != 0, "crealloc(): illegal allocation");
    void *ptr = realloc (x, n);
    vm_assert (ptr != NULL, "`crealloc` failed");
    return ptr;
}

void cfree (void *x) {
    if (x) {
        free (x);
    }
}

char *cstrdup (const char *s) {
#if HAVE_STRDUP
    return strdup (s);
#else
    char *res;
    camemwrite (strlen (s), (u1 **) &res, (u1 *) s, 0);
    return res;
#endif
}

void *_cobjdup (const u1 *buf, size_t len) {
    void *v;
    camemwrite (len, (u1 **) &v, buf, 0);
    return v;
}
