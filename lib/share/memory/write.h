/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_write_h__
#define __cbr_write_h__
#include <stddef.h>
#include "macro.h"
#include "runtime/wtypes.h"

#define deref_set(ptr, x) \
    {                     \
        if (ptr) {        \
            *ptr = x;     \
        }                 \
    }

/* Write `n` bytes from `buf` into `ptr + offset`. NO CHECKS FOR SAFETY! */
unsafe void cmemwrite (size_t n, u1 *ptr, const u1 *buf, size_t offset);

/* Allocate `n` bytes to pointer `ptr` and perform `cmemwrite` with `n` bytes from `buf + offset`. */
void camemwrite (size_t n, u1 **ptr, const u1 *buf, size_t offset);

#endif
