/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_alloc_h__
#define __cbr_alloc_h__
#include <stddef.h>
#include "runtime/wtypes.h"

/* Duplicate struct `s` into struct `&s`. That is, if the argument is of type
`T` this will return an allocated instance of `T *` with the members copied. */
#define cobjdup(s) ((__typeof__ (&s)) _cobjdup ((u1 *) &s, sizeof (s)))

/* Allocates `x` bytes of uninitialized memory. */
void *cmalloc (size_t n);

/* Allocates `x * n` bytes of memory initialized to 0. */
void *ccalloc (size_t n, size_t x);

/* Reallocate pointer `x` to size `n`. */
void *crealloc (void *x, size_t n);

/* Free `x`. */
void cfree (void *x);

/* Duplicate string s. */
char *cstrdup (const char *s);

/* Duplicate the bytes `buf` into a new `void *`. Use `cobjdup` instead. */
void *_cobjdup (const u1 *buf, size_t len);

#endif
