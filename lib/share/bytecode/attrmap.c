/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "attrmap.h"
#include "memory/write.h"
#include "memory/alloc.h"
#include "runtime/vmerror.h"

static u4 attrmap_hash (const u1 *data, size_t len) {
    u4  h = 0;
    u1 *p;
    camemwrite (len, &p, data, 0);

    for (size_t i = 0; i < len; i++) {
        h = 37 * h + *p + i;
    }

    cfree (p);
    return h;
}

int attrmap_new (attrmap *map, size_t n) {
    if (n == 0) {
        return 1;
    }

    *map = (attrmap){ccalloc (sizeof (u1 *), n),
                     ccalloc (sizeof (u1 *), n),
                     ccalloc (sizeof (size_t), n),
                     ccalloc (sizeof (size_t), n),
                     n,
                     0,
                     false};
    return 0;
}

int attrmap_push (attrmap *map, const u1 *key, size_t key_len, u1 *value,
                  size_t value_len, int *op) {
    vm_assert (!map->lock, "map locked!");

    u4 idx = attrmap_hash (key, key_len) % map->size;
    if (idx >= map->count) {
        if (map->count > map->size) {
            return 1;
        }

        map->count++;
        deref_set (op, ATTRMAP_NEWKEY);
        camemwrite (key_len, &map->keys[idx], key, 0);
        camemwrite (value_len, &map->values[idx], value, 0);
        map->keys_len[idx]   = key_len;
        map->values_len[idx] = value_len;
    } else {
        deref_set (op, ATTRMAP_UPDATE);
        cmemwrite (value_len, map->values[idx], value, 0);
        map->values_len[idx] = value_len;
    }

    return 0;
}

u1 *attrmap_get (const attrmap *map, const u1 *key, size_t key_len,
                 size_t *value_len) {
    u4 idx = attrmap_hash (key, key_len) % map->size;
    if (idx >= map->size) {
        deref_set (value_len, 0);
        return NULL;
    } else {
        deref_set (value_len, map->values_len[idx]);
        return map->values[idx];
    }
}

void attrmap_lock (attrmap *map) {
    map->lock = true;
}

void attrmap_free (attrmap *map) {
    if (map->keys) {
        for (size_t i = 0; i < map->count; i++) {
            cfree (map->keys[i]);
        }

        cfree (map->keys);
    }

    if (map->values) {
        for (size_t i = 0; i < map->count; i++) {
            cfree (map->values[i]);
        }

        cfree (map->values);
    }

    cfree (map->keys_len);
    cfree (map->values_len);
}
