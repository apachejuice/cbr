/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_instruction_h__
#define __cbr_instruction_h__
#include "runtime/wtypes.h"
#include "vmconfig.h"

/* Amount of opcodes. */
#define OP_HOWMANY (OP_LAST)

typedef enum {
    OP_NOP    = 0x00,
    OP_POP    = 0x01,
    OP_DUP    = 0x02,
    OP_SWP    = 0x03,
    OP_BIPUSH = 0x04,

    OP_LAST,
} opcode;

/* Represents an instruction. */
typedef struct insn_s instruction;

struct insn_s {
    u1*    args;
    opcode op;
#if DEBUG
    u1 nargs;
#endif
};

#endif
