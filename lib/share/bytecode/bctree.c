/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "bctree.h"
#include "bytecode/bcpool.h"
#include "bytecode/bcsource.h"
#include "bytecode/instruction.h"
#include "macro.h"
#include "runtime/vmerror.h"
#include "runtime/threading/thread.h"
#include "memory/alloc.h"
#include "runtime/order.h"

#include <stddef.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>

static u1 get_u1 (bcsource *src);
static u2 get_u2 (bcsource *src);
static u3 get_u3 (bcsource *src);
static u4 get_u4 (bcsource *src);
static u8 get_u8 (bcsource *src);

static u1       *get_n (bcsource *src, size_t n);
static bcpool   *parse_pool (bcsource *src);
static attrmap  *parse_attrs (bcsource *src);
static function *parse_funcs (bcsource *src, u3 *n);

static nothing parse_error (const bcsource *src, opcode op, const char *msg,
                            ...);

struct bctree bc_parse (bcsource *src) {
    struct bctree tree;

    // parse the header
    const size_t hdrs = sizeof (tree.header);
    memcpy (tree.header, get_n (src, hdrs), hdrs);

    tree.pool      = parse_pool (src);
    tree.glb_attrs = parse_attrs (src);
    tree.glb_funcs = parse_funcs (src, &tree.glb_funcsn);

    return tree;
}

static u1 *get_n (bcsource *src, size_t n) {
    if (!(src->idx + n <= src->len)) {
        parse_error (src, src->src[src->idx - 1], "No bytes left");
    }

    size_t oldidx = src->idx;
    src->idx += n;

    return src->src + oldidx;
}

static bcpool *parse_pool (bcsource *src) {
    u3 len = get_u3 (src);
    if (len == 0) {
        return NULL;
    }

    struct bcpoolent *entries = cmalloc (sizeof (struct bcpoolent) * len);
    for (u3 i = 0; i < len; i++) {
        u1               type = get_u1 (src);
        struct bcpoolent ent;
        ent.id = type;

        switch (type) {
            case BCPOOL_BYTE: {
                ent.P_BYTE = get_u1 (src);
                break;
            }

            case BCPOOL_SHRT: {
                ent.P_SHRT = get_u2 (src);
                break;
            }

            case BCPOOL_INT: {
                ent.P_INT = get_u4 (src);
                break;
            }

            case BCPOOL_LONG: {
                ent.P_LONG = get_u8 (src);
                break;
            }
        }

        entries[i] = ent;
    }

    bcpool *pool = bcpool_new (len);
    bcpool_write (pool, entries);
    return pool;
}

static attrmap *parse_attrs (bcsource *src) {
    u2 len = get_u2 (src);
    if (len == 0) {
        return NULL;
    }

    attrmap *map = cmalloc (sizeof (attrmap));
    attrmap_new (map, len);
    for (u2 i = 0; i < len; i++) {
        u1  key_len   = get_u1 (src);
        u1 *key       = get_n (src, key_len);
        u1  value_len = get_u1 (src);
        u1 *value     = get_n (src, value_len);

#if DEBUG
        int op;
        attrmap_push (map, key, key_len, value, value_len, &op);
        vm_assert (op == ATTRMAP_NEWKEY, "expected new attribute");
#else
        attrmap_push (map, key, key_len, value, value_len, NULL);
#endif
    }

    return map;
}

static u1 get_u1 (bcsource *src) {
    return get_n (src, 1)[0];
}

static u2 get_u2 (bcsource *src) {
    u2 res = 0;
    memcpy (&res, get_n (src, 2), 2);
    return native_ushort_to_cbr (res);
}

static u3 get_u3 (bcsource *src) {
    u1 *d     = get_n (src, 3);
    u1  arr[] = {0, d[0], d[1], d[2]};
    u3  dst;
    memcpy (&dst, arr, sizeof (arr));
    return native_uint_to_cbr (dst);
}

static u4 get_u4 (bcsource *src) {
    u4 res = 0;
    memcpy (&res, get_n (src, 4), 4);
    return native_uint_to_cbr (res);
}

static u8 get_u8 (bcsource *src) {
    u8 res = 0;
    memcpy (&res, get_n (src, 8), 8);
    return native_ulong_to_cbr (res);
}

#if DEBUG
#define MAKE_INSN(a, o, n) ((instruction){a, o, n})
#else
#define MAKE_INSN(a, o, n) ((instruction){a, o})
#endif

#define REQUIRE_STACK_MIN(n)                          \
    do {                                              \
        if (stacksize < n) {                          \
            parse_error (src, id, "Stack underflow"); \
        }                                             \
    } while (0)

#define REQUIRE_ARGS(n)                                                 \
    do {                                                                \
        if (!(src->idx + n <= src->len)) {                              \
            parse_error (src, src->src[src->idx - 1], "No bytes left"); \
        }                                                               \
    } while (0)

static function *parse_funcs (bcsource *src, u3 *n) {
    u3 count = get_u3 (src);
    if (count == 0) {
        return 0;
    }

    *n              = count;
    function *funcs = cmalloc (sizeof (function) * count);
    for (u3 j = 0; j < count; j++) {
        u3       name      = get_u3 (src);
        u3       sig       = get_u3 (src);
        u4       flags     = get_u4 (src);
        u2       localvars = get_u2 (src);
        u2       stackvars = get_u2 (src);
        attrmap *attrs     = parse_attrs (src);
        u3       ninsns    = get_u3 (src);

        instruction *insns     = cmalloc (sizeof (instruction) * ninsns);
        u3           stacksize = 0;
        for (u3 i = 0; i < ninsns; i++) {
            u1 id = get_u1 (src);
            switch (id) {
                case OP_NOP: {
                    insns[i] = MAKE_INSN (NULL, id, 0);
                    break;
                }

                case OP_SWP: {
                    REQUIRE_STACK_MIN (1);
                    insns[i] = MAKE_INSN (NULL, id, 0);
                    break;
                }

                case OP_POP: {
                    REQUIRE_STACK_MIN (1);
                    insns[i] = MAKE_INSN (NULL, id, 0);
                    stacksize--;
                    break;
                }

                case OP_DUP: {
                    REQUIRE_STACK_MIN (1);
                    insns[i] = MAKE_INSN (NULL, id, 0);
                    stacksize++;
                    break;
                }

                case OP_BIPUSH: {
                    REQUIRE_ARGS (1);
                    insns[i] = MAKE_INSN (get_n (src, 1), id, 1);
                    stacksize++;
                    break;
                }

                default: {
                    parse_error (src, id, "Illegal instruction");
                }
            }

            if (stacksize > stackvars) {
                parse_error (src, id, "Stack overflow");
            }
        }

        funcs[j] = (function){name,      sig,   flags,  localvars,
                              stackvars, attrs, ninsns, insns};
    }

    return funcs;
}

static nothing parse_error (const bcsource *src, opcode op, const char *fmt,
                            ...) {
    fprintf (stderr,
             "Error occurred at address %p offset 0x%02x while parsing "
             "bytecode from source "
             "'%s' (on opcode "
             "0x%02x)\n",
             (void *) src->src, src->idx, src->name, op);
    fprintf (stderr, "Message: ");
    va_list ap;
    va_start (ap, fmt);
    vfprintf (stderr, fmt, ap);
    va_end (ap);
    fprintf (stderr, "\n");
    th_exit (NULL);
}

#if DEBUG

static char *bytearray_str (const u1 *array, size_t len) {
    if (array == NULL || len == 0) {
        return cstrdup ("[]");  // must be allocated
    }

    if (len == 1) {
        char *res = cmalloc (sizeof ("[xxx]"));
        sprintf (res, "[%d]", array[0]);
        return res;
    }

    char *res = ccalloc (len * 4 + 2, 1);
    res[0]    = '[';
    for (size_t i = 0; i < len; i++) {
        if (i == 0) {
            sprintf (res + 1, "%d ", array[i]);
        } else if (i == len - 1) {
            sprintf (res + (i * 4 + 1), "%d]", array[i]);
        } else {
            sprintf (res + (i * 4 + 1), "%d ", array[i]);
        }
    }

    return res;
}

void bctree_dump (struct bctree tree) {
    u4 magic;
    memcpy (&magic, tree.header, 4);
    printf ("Magic value: 0x%x\n", native_uint_to_cbr (magic));

    if (tree.pool) {
        printf ("Bytecode pool (length %d):\n", tree.pool->entries_len);
        for (u3 i = 0; i < tree.pool->entries_len; i++) {
            struct bcpoolent ent = tree.pool->entries[i];
            printf ("    #%d [type=0x%02x]\n", i, ent.id);
        }
    }

    if (tree.glb_attrs) {
        printf ("Global attribute table (length %zu):\n",
                tree.glb_attrs->count);
        for (u3 i = 0; i < tree.glb_attrs->count; i++) {
            char *key_str   = bytearray_str (tree.glb_attrs->keys[i],
                                             tree.glb_attrs->keys_len[i]);
            char *value_str = bytearray_str (tree.glb_attrs->values[i],
                                             tree.glb_attrs->values_len[i]);
            printf ("    %s -> %s\n", key_str, value_str);
            cfree (key_str);
            cfree (value_str);
        }
    }

    if (tree.glb_funcs) {
        printf ("Global functions:\n");
        for (u3 i = 0; i < tree.glb_funcsn; i++) {
            function fn = tree.glb_funcs[i];
            printf (
                "    name: #%d sig: #%d flags: 0x%x local: %d stack: %d body: "
                "%d\n",
                fn.name_idx, fn.sig_idx, fn.flags, fn.locvars, fn.stackvars,
                fn.ninsns);

            printf ("    body:\n");
            for (u3 j = 0; j < fn.ninsns; j++) {
                printf ("        0x%x ", fn.insns[j].op);
                if (fn.insns[j].args) {
                    char *b =
                        bytearray_str (fn.insns[j].args, fn.insns[j].nargs);
                    printf ("%s\n", b);
                    cfree (b);
                }
            }
        }
    }
}

#endif
