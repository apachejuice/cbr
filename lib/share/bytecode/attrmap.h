/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_attrmap_h__
#define __cbr_attrmap_h__
#include <stddef.h>
#include <stdbool.h>
#include "runtime/wtypes.h"

#define ATTRMAP_NEWKEY 1
#define ATTRMAP_UPDATE 2

/* Represents a map from byte array keys to byte array values. */
typedef struct attrmap_s attrmap;

/* Create a new attrmap with size n. */
int attrmap_new (attrmap *map, size_t n);
/* Adds a new key to the map. Returns 0 on success, other values on error. If `op` is null,
it is set to `ATTRMAP_NEWKEY` if the key does not exist in the map yet. Otherwise, it will be
set to `ATTRMAP_UDPATE`. This function is NOT mt-safe (one bytecode thread/file). */
int attrmap_push (attrmap *map, const u1 *key, size_t key_len, u1 *value,
                  size_t value_len, int *op);
/* Returns the data at key, null if nonexistent. */
u1 *attrmap_get (const attrmap *map, const u1 *key, size_t key_len,
                 size_t *value_len);
/* Locks the map. This way, it becomes MT-safe. This is also a safety operation, 
as we must ensure that no one is fiddling with the map after locking it. `attrmap_push`
will scream hard at you if such actions were to be committed. */
void attrmap_lock (attrmap *map);

/* Frees the map. */
void attrmap_free (attrmap *map);

struct attrmap_s {
    u1    **values;
    u1    **keys;
    size_t *keys_len;
    size_t *values_len;
    size_t  size;
    size_t  count;
    bool    lock;
};

#endif
