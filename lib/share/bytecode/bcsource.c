/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "bcsource.h"
#include "runtime/vmerror.h"

bcsource bcsource_new (u1 *src, const u4 len, const char *name) {
    vm_assert (src != NULL, "sanity");
    return (bcsource){name, src, len, 0};
}

u1 bcsource_advance (bcsource *src) {
    u4 *idx = &src->idx;
    vm_assert (src && ++*idx < src->len, "out of bounds");
    return src->src[*idx - 1];
}

u1 bcsource_peek (const bcsource *src) {
    return src->src[src->idx];
}

u4 bcsource_left (const bcsource *src, u4 *seen) {
    u4 res = src->len - src->idx;
    if (seen) {
        *seen = src->idx;
    }

    return res;
}
