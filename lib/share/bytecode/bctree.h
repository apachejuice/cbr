/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_bctree_h__
#define __cbr_bctree_h__
#include "bcpool.h"
#include "bcsource.h"
#include "attrmap.h"
#include "bytecode/func.h"
#include "vmconfig.h"

#define BCTREE_HDR_LEN 8

/* Bytecode tree root. */
struct bctree {
    u1        header[BCTREE_HDR_LEN];  // magic + version + reserved
    bcpool   *pool;                    // Constant pool
    attrmap  *glb_attrs;               // Global code attributes
    function *glb_funcs;               // Global functions
    u3        glb_funcsn;              // Amount of global functions
};

// Parse a bytecode tree.
struct bctree bc_parse (bcsource *src);

#if DEBUG
void bctree_dump (struct bctree tree);
#endif

#endif
