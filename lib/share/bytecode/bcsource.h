/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_bcsource_h__
#define __cbr_bcsource_h__
#include "runtime/wtypes.h"

/* Represents a source of bytecode. */
typedef struct bcsource_s bcsource;

/* Create a new source of bytecode. */
bcsource bcsource_new (u1 *src, const u4 len, const char *name);

/* Advance. */
u1 bcsource_advance (bcsource *src);

/* Peek. */
u1 bcsource_peek (const bcsource *src);

/* Check how many bytes left. */
u4 bcsource_left (const bcsource *src, u4 *seen);

struct bcsource_s {
    const char *name;
    u1         *src;
    u4          len;
    u4          idx;
};

#endif
