/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_bcpool_h__
#define __cbr_bcpool_h__
#include "bctypes.h"
#include "runtime/wtypes.h"

enum {
    BCPOOL_BYTE = 0x01,
    BCPOOL_SHRT = 0x02,
    BCPOOL_INT  = 0x03,
    BCPOOL_LONG = 0x04,
};

/* Represents a constant pool. */
typedef struct bcpool_s bcpool;
/* Represents a constant pool entry. */
struct bcpoolent {
    u1 id;
    union {
        u1 P_BYTE;  // 8-bit integer
        u2 P_SHRT;  // 16-bit integer
        u4 P_INT;   // 32-bit integer/floating point
        u8 P_LONG;  // 64-bit integer/floating point
    };
};

/* Creates a new constant pool with the length `n`. */
bcpool *bcpool_new (bcint_t n);

/* Writes the entries to the pool. */
void bcpool_write (bcpool *pool, struct bcpoolent *ent);

/* Destroy the constant pool. */
void bcpool_destroy (bcpool *pool);

struct bcpool_s {
    struct bcpoolent *entries;
    bcint_t           entries_len;
};

#endif
