/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "bcpool.h"
#include "runtime/vmerror.h"
#include "memory/alloc.h"

bcpool *bcpool_new (bcint_t n) {
    bcpool *p      = ccalloc (sizeof (bcpool), 1);
    p->entries_len = n;
    return p;
}

void bcpool_write (bcpool *pool, struct bcpoolent *ent) {
    vm_assert (ent != NULL, "sanity");
    pool->entries = ent;
}

void bcpool_destroy (bcpool *pool) {
    if (pool) {
        cfree (pool->entries);
        cfree (pool);
    }
}
