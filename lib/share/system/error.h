/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_error_h__
#define __cbr_error_h__
#include "platform-types.h"

/* Represents a system error number. */
typedef _CBR_ERRNOTYPE syserror_t;

/* Return the last system error number, similar to `GetLastError` on windows and `errno` on linux. */
syserror_t error_current (void);

/* Convert the error number `err` into a printable string that may be freed with `error_str_free`. */
char *error_str (syserror_t err);

// On linux, strerror returns a `const char*` that cannot be freed and on windows
// `FormatMessageA` allocates a string with `LocalAlloc`. This isn't my favorite way to do this,
// but for this specific situation it handles the issue perfectly.
#if OS_WINDOWS
/* Free an error string. */
#define error_str_free(err) LocalFree (err)
#else
/* Free an error string. */
#define error_str_free(err)
#endif

#endif
