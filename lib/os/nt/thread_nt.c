/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define CBR_THREADING_INTERNAL

#include "runtime/threading/thread.h"
#include "runtime/threading/thread-internal.h"
#include "runtime/threading/critical.h"
#include "memory/alloc.h"
#include <time.h>

#include <windows.h>

static thid_t th_count = 1;              // C main
static thid_t th_cap   = TH_STACK_INIT;  // C main thread handled specially
static thread_local thid_t _th_self;     // Thread-local current thread id
static critical            id_sect;
static INIT_ONCE           _th_init_once = INIT_ONCE_STATIC_INIT;
static DWORD               main_tid;
static LPVOID             *return_stack;

static unsigned long     _default_th_lifecycle (void *_in_a);
static void              init_th_base (threadBase **b, const threadBase *parent,
                                       const char *name);
static BOOL CALLBACK     _th_init_cb (PINIT_ONCE once, PVOID data, PVOID *ctx);
static void              define_thread (threadBase *tinfo);
static vmThreadInitArgs *create_init_args (threadBase *b, bundle *options,
                                           vmThreadPreExecCB  pre_cb,
                                           vmThreadPostExecCB post_cb,
                                           vmThreadMainFunc   main);

static thid_t create_id () {
    static thid_t id = 0;
    critical_enter (&id_sect);
    thid_t result = ++id;  // no id 0; it is reserved
    th_count++;            // Assume we're creating a new thread
    critical_exit (&id_sect);
    return result;
}

void th_init () {
    InitOnceExecuteOnce (&_th_init_once, &_th_init_cb, NULL, NULL);
}

void th_deinit () {
    free (main_thread_info);
    free (th_stack);
    critical_destroy (&id_sect);
}

void th_sleep (millis_t n) {
    Sleep (n);
}

threadBase *th_self (void) {
    DWORD self = GetCurrentThreadId ();
    if (self == main_tid) {  // IDs are unique, i think?
        return main_thread_info;
    } else {
        return th_stack[_th_self - 1];  // 0 is for C main
    }
}

double th_join (const threadBase *th, void **data) {
    time_t t = time (NULL);
    WaitForSingleObject (th->_data, INFINITE);
    if (data) {
        *data = return_stack[th->vm_id - 1];
    }

    return difftime (time (NULL), t);
}

vmThread *vmt_new (const char *name, vmThreadPreExecCB pre_cb,
                   vmThreadPostExecCB post_cb, notnull vmThreadMainFunc main,
                   const threadBase *parent, thid_t *id) {
    vmThread *th = cmalloc (sizeof (vmThread));
    init_th_base (&th->_p, parent, name);
    th->pre_cb  = pre_cb == NULL ? _default_preexec : pre_cb;
    th->post_cb = post_cb == NULL ? _default_postexec : post_cb;
    th->main    = main;

    if (id) {
        *id = th->_p->vm_id;
    }

    define_thread (th->_p);
    return th;
}

inline threadBase *vmt_base (const vmThread *th) {
    return th->_p;
}

int vmt_run (vmThread *th, bundle *arg) {
    if (!th || has_flag (th->_p->_flags, THF_DEAD) ||
        !has_flag (th->_p->_flags, THF_NEW)) {
        return TH_STATUS_AGAIN;
    }

    DWORD  thid;
    HANDLE winth = CreateThread (
        NULL, 0, &_default_th_lifecycle,
        create_init_args (th->_p, arg, th->pre_cb, th->post_cb, th->main), 0,
        &thid);

    th->_p->_data = (void *) winth;
    th->_p->os_id = (nthid_t) thid;
    return 0;
}

void vmt_free (vmThread *th) {
    CloseHandle ((HANDLE) th->_p->_data);
    free (th->_p);
    free (th);
}

static vmThreadInitArgs *create_init_args (threadBase *b, bundle *options,
                                           vmThreadPreExecCB  pre_cb,
                                           vmThreadPostExecCB post_cb,
                                           vmThreadMainFunc   main) {
    vmThreadInitArgs *a = cmalloc (sizeof (vmThreadInitArgs));
    a->t                = b;
    a->options          = options;
    a->pre_cb           = pre_cb;
    a->post_cb          = post_cb;
    a->main             = main;

    return a;
}

static unsigned long _default_th_lifecycle (void *_in_a) {
    vmThreadInitArgs *in_a = (vmThreadInitArgs *) _in_a;
    th_override_flag (in_a->t, THF_PREXC);
    in_a->pre_cb (in_a->t);
    th_override_flag (in_a->t, THF_MAIN);
    in_a->main (in_a->t, in_a->options);
    th_override_flag (in_a->t, THF_POSTXC);
    in_a->post_cb (in_a->t);
    th_override_flag (in_a->t, THF_DEAD);

    bundle *result                   = in_a->options;
    return_stack[in_a->t->vm_id - 1] = result;
    free (in_a);

    return 0;
}

static BOOL CALLBACK _th_init_cb (notused PINIT_ONCE once, notused PVOID data,
                                  notused PVOID *ctx) {
    main_tid = GetCurrentThreadId ();

    main_thread_info         = cmalloc (sizeof (threadBase));
    main_thread_info->_flags = THF_MAIN;
    main_thread_info->os_id  = main_tid;
    main_thread_info->vm_id  = 0;
    main_thread_info->parent = NULL;
    main_thread_info->name   = TH_MAIN_NAME;

    id_sect      = critical_new ();
    th_stack     = cmalloc (sizeof (threadBase) * TH_STACK_INIT);
    return_stack = cmalloc (sizeof (LPVOID) * TH_STACK_INIT);
    return TRUE;
}

static void init_th_base (threadBase **b, const threadBase *parent,
                          const char *name) {
    threadBase *tb = cmalloc (sizeof (threadBase));
    tb->_flags     = THF_NEW;
    tb->name       = name;
    tb->os_id      = 0;
    tb->vm_id      = create_id ();
    tb->parent     = parent;

    *b = tb;
}

static void define_thread (threadBase *tinfo) {
    if (th_count == th_cap) {
        th_cap++;
        th_stack = realloc (th_stack, sizeof (threadBase) * th_cap);
    }

    th_count++;
    th_stack[th_count - 1] = tinfo;
}
