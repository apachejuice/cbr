/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <windows.h>
#include <stdio.h>
#include "io/console.h"
#include "runtime/vmerror.h"

#define ANSI_SGR_START "\033["
#define ANSI_SGR_END   "m"

console console_stdout () {
    return GetStdHandle (STD_OUTPUT_HANDLE);
}

console console_stderr () {
    return GetStdHandle (STD_ERROR_HANDLE);
}

size_t console_lwrite (console *con, const void *buf, size_t n) {
    if (!n || !buf || !con || *con == INVALID_HANDLE_VALUE) return 0;
    DWORD out;
    DWORD res = WriteConsole (*con, buf, n, &out, NULL);
    return res ? out : 0;
}

size_t console_write (console *con, const char *str) {
    return console_lwrite (con, str, strlen (str));
}

bool console_set_text_color (console *con, int red, int green, int blue) {
    if (!IS_COLOR (red) || !IS_COLOR (green) || !IS_COLOR (blue)) {
        return false;
    }

    DWORD ver = GetVersion ();
    if ((DWORD) (LOBYTE (LOWORD (ver))) < 10) {
        // Windows pre-10 does things differently so lets try to estimate.
        DWORD flags = 0;

        // if one of them is the only one, use that.
        if (red && (!green && !blue)) {
            flags = FOREGROUND_RED;
        } else if (green && (!red && !blue)) {
            flags = FOREGROUND_GREEN;
        } else if (blue && (!red && !green)) {
            flags = FOREGROUND_BLUE;
        } else {
            // merge two of them
            if (red && green && !blue) {
                flags = FOREGROUND_RED | FOREGROUND_GREEN;
            } else if (red && blue && !green) {
                flags = FOREGROUND_RED | FOREGROUND_BLUE;
            } else if (blue && green && !red) {
                flags = FOREGROUND_BLUE | FOREGROUND_GREEN;
            } else {
                // now all are over 0 or 0
                if (!green) {
                    return true;
                } else {
                    flags = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN;
                }
            }
        }

        SetConsoleTextAttribute (*con, flags);
    } else {
        // windows >= 10 supports ansi escapes
        // literally copied from console_linux.c
        char command[20];
        sprintf (command, "%s38;2;%d;%d;%d%s", ANSI_SGR_START, red, green, blue,
                 ANSI_SGR_END);
        return truth (console_write (con, command));
    }

    return true;
}

void console_reset_text_color (console *con) {
    DWORD ver = GetVersion ();
    if (((DWORD) LOBYTE (LOWORD (ver))) < 10) {
        SetConsoleTextAttribute (*con, 0);
    } else {
        console_write (con, ANSI_SGR_START "0" ANSI_SGR_END);
    }
}
