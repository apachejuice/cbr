/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "io/file.h"
#include "runtime/vmerror.h"
#include "memory/alloc.h"
#include "system/error.h"
#include <Windows.h>
#include <handleapi.h>

u1 *read_file (const char *file, size_t *len, bool txt, fileError *err) {
    // We specifically want CreateFileA here since we're reading a binary file.
    HANDLE f = CreateFileA (file, GENERIC_READ, FILE_SHARE_READ, NULL,
                            OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (f == INVALID_HANDLE_VALUE) {
        DWORD e = GetLastError ();
        // TODO: I don't exactly know the kinds of errors CreateFileA sets, so if anyone has a
        // better idea of what I'm missing (or if there's a redundant check here) please tell me.
        if (e == ERROR_FILE_NOT_FOUND) {
            *err = F_ERR_NOT_FOUND;
        } else if (e == ERROR_ACCESS_DENIED || e == ERROR_INVALID_ACCESS ||
                   e == ERROR_WRITE_PROTECT) {
            *err = F_ERR_NOREAD;
        } else if (e == ERROR_OUTOFMEMORY || e == ERROR_NOT_ENOUGH_MEMORY) {
            *err = F_ERR_2BIG;
        } else {
            *err = F_ERR_NOKNOW;
        }

        if (F_ERR_KNOWN (e)) {
            return NULL;
        }

        vm_assert (f != INVALID_HANDLE_VALUE, error_str (e));
    }

    DWORD buf_len  = GetFileSize (f, NULL);
    u1   *read_buf = cmalloc (sizeof (u1) * (buf_len + (txt ? 1 : 0)));
    DWORD read_len = 0;
    vm_assert (ReadFile (f, read_buf, buf_len, &read_len, NULL),
               "Failed to read file after CreateFile succeeded");
    if (txt) {
        read_buf[buf_len] = 0;
    }

    *err = F_ERR_SUCCESS;
    *len = read_len;
    return read_buf;
}

static DWORD win32_make_access (int mode) {
    if (mode & FM_READ) {
        return GENERIC_READ;
    } else if (mode & FM_WRITE) {
        return GENERIC_WRITE | GENERIC_READ;
    } else {  // append | create
        return GENERIC_WRITE | GENERIC_READ;
    }
}

static DWORD win32_make_create (int mode) {
    if (mode & FM_CREATE) {
        return CREATE_ALWAYS;
    } else if (mode & FM_WRITE) {
        return TRUNCATE_EXISTING;
    } else {
        return OPEN_EXISTING;
    }
}

int file_open_simple (file *handle, const char *path, int mode) {
    if (path == NULL || !file_mode_check (mode) || strlen (path) < 2) {
        return ERROR_BAD_ARGUMENTS;
    } else if (handle == NULL || *handle == NULL) {
        *handle = INVALID_HANDLE_VALUE;
    }

    bool rel = path[0] == '/';
    if (rel && strlen (path) < 3) {
        return ERROR_BAD_ARGUMENTS;
    }

    HANDLE f =
        CreateFileA (path, win32_make_access (mode), FILE_SHARE_READ, NULL,
                     win32_make_create (mode), FILE_ATTRIBUTE_NORMAL, NULL);
    if (f == INVALID_HANDLE_VALUE) {
        *handle = NULL;
        return GetLastError ();
    }

    *handle = f;
    return 0;
}

int file_close (file *handle) {
    if (handle == NULL || *handle == NULL || *handle == INVALID_HANDLE_VALUE) {
        return 1;
    } else {
        CloseHandle (*handle);
        return 0;
    }
}
