/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdbool.h>

#include "runtime/threading/critical.h"
#include <Windows.h>

critical critical_new () {
    critical c;
    c.entered = false;
    InitializeCriticalSection (&c.crit);
    return c;
}

void critical_enter (critical *critical) {
    critical->entered = true;
    EnterCriticalSection (&critical->crit);
}

void critical_exit (critical *critical) {
    critical->entered = false;
    LeaveCriticalSection (&critical->crit);
}

void critical_destroy (critical *critical) {
    DeleteCriticalSection (&critical->crit);
}
