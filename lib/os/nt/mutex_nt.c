/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "runtime/threading/mutex.h"
#include "memory/alloc.h"
#include <Windows.h>

mutex *mutex_new () {
    mutex *mtx  = cmalloc (sizeof (mutex));
    mtx->mtx    = CreateMutexA (NULL, FALSE, NULL);
    mtx->locked = false;
    return mtx;
}

bool mutex_lock (mutex *mtx) {
    if (mtx->locked) {
        return false;
    }

    DWORD res = WaitForSingleObject (mtx->mtx, INFINITE);
    if (FAILED (res)) {
        /* TODO: VM-wide error handing system, kind of like the JVM's crash dump? */
    }

    return true;
}

void mutex_unlock (mutex *mtx) {
    ReleaseMutex (mtx->mtx);
}

void mutex_destroy (mutex *mtx) {
    CloseHandle (mtx->mtx);
}
