/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "runtime/threading/tls.h"
#include "runtime/vmerror.h"

inline tls tls_new () {
    return TlsAlloc ();
}

inline tls tls_new_data (void *d) {
    tls s = tls_new ();
    TlsSetValue (s, d);
    return s;
}

inline void *tls_get_data (tls *s, bool *err) {
    void *d = TlsGetValue (*s);
    if (d == 0 && GetLastError () != ERROR_SUCCESS) {
        *err = true;
        return NULL;
    }

    return d;
}

void tls_set_data (tls *s, void *d) {
    vm_assert (TlsSetValue (*s, d) != 0, "`TlsSetValue` failed");
}

void tls_free (tls *s) {
    vm_assert (TlsFree (*s) != 0, "`TlsFree` failed");
}
