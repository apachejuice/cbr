/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "runtime/threading/condition.h"
#include "macro.h"

condition condition_new () {
    CONDITION_VARIABLE c;
    InitializeConditionVariable (&c);
    return c;
}

bool condition_sleep_forever (condition *cond, critical *crit) {
    return condition_sleep (cond, crit, INFINITE);
}

bool condition_sleep (condition *cond, critical *crit, millis_t n) {
    if (!crit->entered) {
        return false;
    }

    WINBOOL res = SleepConditionVariableCS (cond, &crit->crit, n);
    if (res) {
        return true;
    }

    return false;
}

void condition_wake (condition *cond) {
    WakeConditionVariable (cond);
}

void condition_wake_all (condition *cond) {
    WakeAllConditionVariable (cond);
}

void condition_destroy (notused condition *cond) {
    /* Not needed on win32 */
}
