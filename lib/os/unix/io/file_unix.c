/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "io/file.h"
#include "memory/alloc.h"
#include "runtime/vmerror.h"
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

static fileError map_errno (int _errno) {
    if (_errno == ENOENT) {
        return F_ERR_NOT_FOUND;
    } else if (_errno == EROFS || _errno == EACCES) {
        return F_ERR_NOREAD;
    } else if (_errno == EOVERFLOW) {
        return F_ERR_2BIG;
    } else if (_errno == EISDIR) {
        return F_ERR_ISDIR;
    } else {
        return F_ERR_NOKNOW;
    }
}

static int map_filemode (int mode) {
    int result = 0;
    if (mode & FM_WRITE) {
        result = O_WRONLY;
    } else if (mode & FM_READ) {
        result = O_RDONLY;
    } else {
        result = O_RDWR | O_APPEND;
    }

    if (mode & FM_CREATE) {
        result |= O_CREAT;
    }

    return result;
}

u1 *read_file (const char *file, size_t *len, bool txt, fileError *err) {
    FILE *f = fopen (file, "rb");
    if (!f) {
        fileError e = map_errno (errno);
        if (F_ERR_KNOWN (e)) {
            *err = e;
            return NULL;
        }

        vm_assert (f != NULL, strerror (errno));
    }

    fseek (f, 0, SEEK_END);
    size_t flen = ftell (f);
    rewind (f);

    u1 *buffer = cmalloc ((flen + (txt ? 1 : 0)) * sizeof (u1));
    vm_assert (buffer != NULL, "NULL buffer");
    fread (buffer, flen, 1, f);
    fclose (f);
    if (txt) {
        buffer[flen] = 0;
    }

    *len = flen;
    *err = F_ERR_SUCCESS;
    return buffer;
}

int file_open_simple (file *handle, const char *path, int mode) {
    int fm = map_filemode (mode);
    if (!file_mode_check (mode)) {
        return EINVAL;
    }

    int desc;
    if (mode & FM_CREATE) {
        desc = open (path, fm, S_IRWXU);
    } else {
        desc = open (path, fm);
    }

    *handle = (file){fm, desc};
    return 0;
}

int file_close (file *handle) {
    return close (handle->hndl);
}

int file_read_simple (file *handle, u1 **buf, size_t *size) {
    vm_assert (handle != NULL, "sanity");

    off_t fsize = lseek (handle->hndl, 0, SEEK_END);
    if (fsize == -1) { // oops fuck said my friend
        *buf  = NULL;
        *size = 0;
        return errno;
    }

    lseek (handle->hndl, 0, SEEK_SET);  // reset the descriptor
    if (fsize == 0) {
        *buf  = NULL;
        *size = 0;
        return 0;
    }

    u1     *cnt = cmalloc (fsize);
    ssize_t res = read (handle->hndl, cnt, fsize);
    if (res == -1) {
        return errno;
    } else if (res == 0) {
        *size = 0;
        *buf  = NULL;
    } else {
        *size = fsize;
        *buf  = cnt;
    }

    return 0;
}

int file_write_simple (file *handle, const u1 *data, size_t len) {
    vm_assert (handle != NULL, "sanity");
    ssize_t res = write (handle->hndl, data, len);
    if (res == -1) {
        return errno;
    } else {
        return 0;
    }
}
