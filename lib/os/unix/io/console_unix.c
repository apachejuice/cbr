/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "io/console.h"
#include "macro.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define ANSI_SGR_START "\033["
#define ANSI_SGR_END   "m"

console console_stdout (void) {
    return 1;
}

console console_stderr (void) {
    return 2;
}

size_t console_write (console *con, const char *str) {
    return console_lwrite (con, str, strlen (str));
}

size_t console_lwrite (console *con, const void *buf, size_t n) {
    if (!n || !buf || !con) return 0;

    int res = write (*con, buf, n);
    if (res == -1) {
        return 0;
    } else {
        return n;
    }
}

bool console_set_text_color (console *con, int red, int green, int blue) {
    if (!con || !IS_COLOR (red) || !IS_COLOR (green) || !IS_COLOR (blue)) {
        return false;
    }

    // longest possible text color code being "\038[38;2;255;255;255m"
    char command[20];
    sprintf (command, "%s38;2;%d;%d;%d%s", ANSI_SGR_START, red, green, blue,
             ANSI_SGR_END);
    return truth (console_write (con, command));
}

void console_reset_text_color (console *con) {
    if (!con) return;
    console_write (con, ANSI_SGR_START "0" ANSI_SGR_END);
}
