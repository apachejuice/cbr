/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <pthread.h>
#include "runtime/threading/critical.h"
#include "runtime/threading/mutex.h"

critical critical_new () {
    pthread_mutex_t     mtx;
    pthread_mutexattr_t attr;
    pthread_mutexattr_init (&attr);
    pthread_mutexattr_settype (&attr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init (&mtx, &attr);

    return (critical){.mtx = mtx, .attr = attr, .entered = false};
}

void critical_enter (critical *critical) {
    critical->entered = true;
    pthread_mutex_lock (&critical->mtx);
}

void critical_exit (critical *critical) {
    critical->entered = false;
    pthread_mutex_unlock (&critical->mtx);
}

void critical_destroy (critical *critical) {
    pthread_mutexattr_destroy (&critical->attr);
    pthread_mutex_destroy (&critical->mtx);
}
