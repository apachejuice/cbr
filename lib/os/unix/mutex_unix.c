/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License"){}
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "runtime/threading/mutex.h"
#include "memory/alloc.h"
#include <stdlib.h>
#include <pthread.h>

mutex *mutex_new () {
    pthread_mutex_t     mtx;
    pthread_mutexattr_t attr;
    pthread_mutexattr_init (&attr);
    pthread_mutexattr_settype (&attr, PTHREAD_MUTEX_NORMAL);
    pthread_mutex_init (&mtx, &attr);

    mutex *m = cmalloc (sizeof (mutex));
    m->attr  = attr;
    m->mtx   = mtx;
    return m;
}

void mutex_destroy (mutex *mtx) {
    pthread_mutexattr_destroy (&mtx->attr);
    pthread_mutex_destroy (&mtx->mtx);
}

inline bool mutex_lock (mutex *mtx) {
    int res = pthread_mutex_trylock (&mtx->mtx);
    return !res;
}

inline void mutex_unlock (mutex *mtx) {
    pthread_mutex_unlock (&mtx->mtx);
}
