/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define CBR_THREADING_INTERNAL

#include "runtime/threading/thread.h"
// must keep threads.h before vmthread-internal.h

#if OS_LINUX
#include <threads.h>  // thread_local, not used for the threading implementation
#endif

#include "runtime/threading/thread-internal.h"
#include "runtime/threading/critical.h"
#include "memory/alloc.h"
#include "timespec.h"
#include "macro.h"
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
// Unix-specifix thread implementation.

static void              init_th_base (threadBase **b, const threadBase *parent,
                                       const char *name);
static thid_t            create_id ();
static void              define_thread (threadBase *tinfo);
static void              _default_th_init ();
static void             *_default_th_lifecycle (void *_in_a);
static vmThreadInitArgs *create_init_args (threadBase *b, bundle *options,
                                           vmThreadPreExecCB  pre_cb,
                                           vmThreadPostExecCB post_cb,
                                           vmThreadMainFunc   main);
static pthread_once_t    _default_onceinit = PTHREAD_ONCE_INIT;

static thid_t th_count = 1;              // C main
static thid_t th_cap   = TH_STACK_INIT;  // C main thread handled specially
static thread_local thid_t _th_self;     // Thread-local current thread id
static critical            id_sect;
static pthread_t           main_tid;

void th_init () {
    pthread_once (&_default_onceinit, _default_th_init);
}

void th_deinit () {
    free (main_thread_info);
    free (th_stack);
    critical_destroy (&id_sect);
}

threadBase *th_self (void) {
    pthread_t self = pthread_self ();
    if (pthread_equal (self, main_tid)) {
        return main_thread_info;
    } else {
        return th_stack[_th_self - 1];  // 0 is for C main
    }
}

void th_sleep (millis_t n) {
    const struct timespec ts = timespec_from_ms (n);
    nanosleep (&ts, NULL);
}

nothing th_exit (void *ptr) {
    pthread_exit (ptr);
}

double th_join (const threadBase *th, void **data) {
    time_t t = time (NULL);
#if OS_DARWIN
    pthread_join (pthread_from_mach_thread_np (th->os_id), data);
#else
    pthread_join (th->os_id, data);
#endif
    return difftime (time (NULL), t);
}

vmThread *vmt_new (const char *name, vmThreadPreExecCB pre_cb,
                   vmThreadPostExecCB post_cb, vmThreadMainFunc main,
                   const threadBase *parent, thid_t *id) {
    vmThread *th = cmalloc (sizeof (vmThread));
    init_th_base (&th->_p, parent, name);
    th->pre_cb  = pre_cb == NULL ? _default_preexec : pre_cb;
    th->post_cb = post_cb == NULL ? _default_postexec : post_cb;
    th->main    = main;

    if (id) {
        *id = th->_p->vm_id;
    }

    define_thread (th->_p);
    return th;
}

int vmt_run (vmThread *th, bundle *arg) {
    if (th == NULL || has_flag (th->_p->_flags, THF_DEAD) ||
        !has_flag (th->_p->_flags, THF_NEW)) {
        return TH_STATUS_AGAIN;
    }

    pthread_t n;
    int       s = pthread_create (
              &n, NULL, _default_th_lifecycle,
              create_init_args (th->_p, arg, th->pre_cb, th->post_cb, th->main));
    if (s) {
        return s;
    }

    th->_p->os_id = (nthid_t) n;
    return 0;
}

threadBase *vmt_base (const vmThread *th) {
    return th->_p;
}

void vmt_free (vmThread *th) {
    free (th->_p);
    free (th);
}

static void init_th_base (threadBase **b, const threadBase *parent,
                          const char *name) {
    threadBase *tb = cmalloc (sizeof (threadBase));
    tb->_flags     = THF_NEW;
    tb->name       = name;
    tb->os_id      = 0;
    tb->vm_id      = create_id ();
    tb->parent     = parent;

    *b = tb;
}

// This function has no checks for bounds, those should be handled by some higher-level future construct
static thid_t create_id () {
    static thid_t id = 0;
    critical_enter (&id_sect);
    thid_t result = ++id;  // no id 0; it is reserved
    th_count++;            // Assume we're creating a new thread
    critical_exit (&id_sect);
    return result;
}

static void *_default_th_lifecycle (void *_in_a) {
    vmThreadInitArgs *in_a = (vmThreadInitArgs *) _in_a;
    th_override_flag (in_a->t, THF_PREXC);
    in_a->pre_cb (in_a->t);
    th_override_flag (in_a->t, THF_MAIN);
    in_a->main (in_a->t, in_a->options);
    th_override_flag (in_a->t, THF_POSTXC);
    in_a->post_cb (in_a->t);
    th_override_flag (in_a->t, THF_DEAD);

    bundle *result = in_a->options;
    free (in_a);
    return result;
}

static void _default_th_init () {
    main_tid = pthread_self ();

    // Main thread info is constant
    main_thread_info         = cmalloc (sizeof (threadBase));
    main_thread_info->_flags = THF_MAIN;
#if OS_DARWIN
    main_thread_info->os_id = pthread_mach_thread_np (main_tid);
#else
    main_thread_info->os_id = main_tid;
#endif
    main_thread_info->vm_id  = 0;
    main_thread_info->parent = NULL;
    main_thread_info->name   = TH_MAIN_NAME;

    id_sect  = critical_new ();
    th_stack = cmalloc (sizeof (threadBase) * TH_STACK_INIT);
}

static vmThreadInitArgs *create_init_args (threadBase *b, bundle *options,
                                           vmThreadPreExecCB  pre_cb,
                                           vmThreadPostExecCB post_cb,
                                           vmThreadMainFunc   main) {
    vmThreadInitArgs *a = cmalloc (sizeof (vmThreadInitArgs));
    a->t                = b;
    a->options          = options;
    a->pre_cb           = pre_cb;
    a->post_cb          = post_cb;
    a->main             = main;

    return a;
}

static void define_thread (threadBase *tinfo) {
    if (th_count == th_cap) {
        th_cap++;
        th_stack = realloc (th_stack, sizeof (threadBase) * th_cap);
    }

    th_count++;
    th_stack[th_count - 1] = tinfo;
}
