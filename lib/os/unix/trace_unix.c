/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memory/alloc.h"
#include "runtime/crash/trace.h"
#include <execinfo.h>

char **dbg_trace (int n, int *out) {
    void **buf = cmalloc (n * sizeof (void *));
    int    s   = backtrace (buf, n);
    *out       = s;
    return backtrace_symbols (buf, s);
}
