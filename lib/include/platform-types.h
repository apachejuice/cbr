/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// clang-format off
#ifndef __cbr_platform_types_h__
#define __cbr_platform_types_h__
#include "vmconfig.h"

#include <stdbool.h>

#ifndef __cplusplus
#include <stdint.h>
#else
#include <cstdint>
#endif

#if OS_WINDOWS
#   include <Windows.h>
#   define _CBR_THREADID        unsigned long long int
#   define _CBR_THREADTYPE      HANDLE
#   define _CBR_THREADFMT       "%llu"
#   define _CBR_MUTEXTYPE       struct { HANDLE mtx; bool locked; }
#   define _CBR_CRITICALTYPE    struct { CRITICAL_SECTION crit; bool entered; }
#   define _CBR_CONDTYPE        CONDITION_VARIABLE
#   define _CBR_BARRIERTYPE     SYNCHRONIZATION_BARRIER
#   define _CBR_SLEEPTIMETYPE   DWORD
#   define _CBR_PIDTYPE         DWORD
#   define _CBR_THLOCALTYPE     DWORD
#   define _CBR_ERRNOTYPE       DWORD
#   define _CBR_CONSOLETYPE     HANDLE
#   define _CBR_FILEHANDLETYPE  HANDLE

#   define CPID_FMT             "%lu"
#   define pidf(pid)            (pid)
#elif OS_LINUX
#   include <pthread.h>
#   include "runtime/wtypes.h"
#   define _CBR_THREADID        unsigned long int
#   define _CBR_THREADTYPE      void* __attribute__((unused))
#   define _CBR_THREADFMT       "%lu"
#   define _CBR_MUTEXTYPE       struct { pthread_mutex_t mtx; pthread_mutexattr_t attr; }
#   define _CBR_CRITICALTYPE    struct { pthread_mutex_t mtx; pthread_mutexattr_t attr; bool entered; }
#   define _CBR_CONDTYPE        pthread_cond_t
#   define _CBR_SLEEPTIMETYPE   u4
#   define _CBR_PIDTYPE         pid_t
#   define _CBR_ERRNOTYPE       int
#   define _CBR_BARRIERTYPE     pthread_barrier_t
#   define _CBR_THLOCALTYPE     pthread_key_t
#   define _CBR_CONSOLETYPE     int
#   define _CBR_FILEHANDLETYPE  int

#   define CPID_FMT             "%d"
#   define pidf(pid)            ((unsigned) pid)
#elif OS_DARWIN
#   include <pthread.h>
#   include "runtime/wtypes.h"
#   include <sys/types.h>

#   define _CBR_THREADID        mach_port_t
#   define _CBR_THREADTYPE      void* __attribute__((unused))
#   define _CBR_THREADFMT       "%lu"
#   define _CBR_MUTEXTYPE       struct { pthread_mutex_t mtx; pthread_mutexattr_t attr; }
#   define _CBR_CRITICALTYPE    struct { pthread_mutex_t mtx; pthread_mutexattr_t attr; bool entered; }
#   define _CBR_CONDTYPE        pthread_cond_t
#   define _CBR_SLEEPTIMETYPE   u4
#   define _CBR_PIDTYPE         pid_t
#   define _CBR_ERRNOTYPE       int
#   define _CBR_BARRIERTYPE     pthread_barrier_t
#   define _CBR_THLOCALTYPE     pthread_key_t
#   define _CBR_CONSOLETYPE     int
#   define _CBR_FILEHANDLETYPE  int

#   define CPID_FMT             "%d"
#   define pidf(pid)            ((unsigned) pid)
#endif

/* Millisecond type. */
typedef _CBR_SLEEPTIMETYPE millis_t;

#endif
// clang-format on
