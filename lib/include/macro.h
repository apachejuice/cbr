/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_macro_h__
#define __cbr_macro_h__
#include "vmconfig.h"

#define boolean(val)       ((val) ? "true" : "false")
#define truth(val)         ((val) ? true : false)
#define notnull            __attribute__ ((nonnull))
#define has_flag(src, val) (((src) & (val)) != 0)
#define notused            __attribute__ ((unused))
#define nothing            void __attribute__ ((noreturn))
#define api_export         __attribute__ ((dllexport))
#define unsafe                                                                \
    __attribute__ ((                                                          \
        warning ("This function has been deemed memory-unsafe and should be " \
                 "used with care.")))
#define LEN_ARGS(t, ...) (sizeof ((t[]){__VA_ARGS__}) / sizeof (t))

// memory multipliers
#define K 1024
#define M (K * K)

#endif
