/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __cbr_perhaps_h__
#define __cbr_perhaps_h__
#include <stdbool.h>
#include "vmconfig.h"

#define perhaps(ct, tag)                            \
    struct __uniquePerhapsImplOf_##ct##_For_##tag { \
        bool present;                               \
        ct   value;                                 \
    }

#define is_present(perhaps)             (perhaps.present)
#define perhaps_empty(phstruct)         ((phstruct){false})
#define perhaps_val(phstruct, val)      ((phstruct){true, val})
#define get_value(perhaps)              (perhaps.value)
#define get_value_typed(perhaps, ctype) ((ctype) perhaps.value)
#define perhaps_exchange(perhaps, bupval) \
    (is_present (perhaps) ? perhaps.value : bupval)
#define perhaps_let(perhaps, blk) perhaps_let_named (perhaps, it, blk)

#define perhaps_let_named(perhaps, name, blk)            \
    {                                                    \
        if (is_present (perhaps)) {                      \
            typeof (perhaps.value) name = perhaps.value; \
            blk;                                         \
        }                                                \
    }

#define perhaps_set(perhaps, _value) \
    {                                \
        if (!is_present (perhaps)) { \
            perhaps.value = _value   \
        }                            \
    }

#endif
