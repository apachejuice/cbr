default:
	@echo "Available targets: test build-for-me-because-meson-so-hard"

build-for-me-because-meson-so-hard:
	@echo "Really that hard?"
	@sleep 1
	@meson build --reconfigure
	@ninja -C build

test:
	./build/tests/bundle-test
	./build/tests/attrmap-test
