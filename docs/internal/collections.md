# Collections

### Table of contents:
  - [Background](#background)
  - [The bundle](#the-bundle)
    - [Currently used in](#where-is-the-bundle-used)
    - [Supported types](#supported-types)
    - [API index](#api-index)
    - [Content note](#content-note)

## Background
To write efficient code and pass data around easily, one needs collections. The CBR currently only ships with one collection data type, `bundle`. The collection headers reside in `lib/collections`. For example, `bundle` is defined in `lib/collections/bundle.h`.

<br>

# The bundle
The bundle acts much like the `android.os.Bundle` class in android programming: It is a type-flexible hash table. What does that mean? It means that if I put a key of type `double` in a bundle and try to query it as an `int`, it wont work. Here's an example:
```c
#include "collections/bundle.h"
#include <stdio.h>

int main (int argc, char *argv[]) {
    bundle *b = bundle_create ();
    double amount;
    double value = 1.23;

    // Place the value into the bundle
    bundle_put_double (b, "my-money", value);
    // Now query it
    bundle_get_double (b, "my-money", &amount);

    printf ("%f\n", amount); // prints 1.23

    // Now make an incorrect query
    int x;
    int res = bundle_get_int (b, "my-money", &x);
    if (res) { // bundle_get_* returns a nonzero value on error
        printf ("No integer key 'my-money'\n");
    }

    return 0;
}
```

## Where is the bundle used?
A good example of real-world usage of the bundle can be found in [thread.h](https://github.com/apachejuice/cbr/blob/dev-staging/lib/runtime/threading/thread.h):
```c
int vmt_run (vmThread *th, bundle *arg);
```
The `vmt_run` function takes a `bundle` as an argument to pass options to the thread. It is passed as a pointer, so the caller and the thread method can communicate using the bundle as a data carrier. However, both the thread and the caller must know exactly _what_ data they are expecting to get.

## Supported types
Currently the bundle supports the following types:
- `int`
- `long`
- `long long` (name: `long_long`)
- `float`
- `double`
- `char *` (name: `str`)
- `void *` (name: `voidp`)
- `bundle *` (name: `bundle`)

If an immense need for more supported types arises, they can easily be added to the bundle.

## API index
  - [Includes](#includes)
  - [Types:](#types)
    - [`bundle`](#bundle)
    - [`bundleHashFunc`](#bundlehashfunc)
  - [Methods:](#methods)
    - [`bundle_create (void): bundle *`](#bundle-bundlecreate-void)
    - [`bundle_create_hf (bundleHashFunc): bundle *`](#bundle-bundlecreatehf-bundlehashfunc-hf)
    - [`bundle_size (const bundle *): size_t`](#sizet-bundlesize-const-bundle-b)
    - [`bundle_merge (bundle *, const bundle *, size_t): size_t`](#sizet-bundlemerge-bundle-dst-const-bundle-src-sizet-nexist)
    - [`bundle_has_key (const bundle *, const char *): bool`](#bool-bundlehaskey-const-bundle-b-const-char-key)
    - [`bundle_destroy (bundle *): void`](#void-bundledestroy-bundle-b)
    - [`bundle_put_<type> (bundle *, const char *, <type> *): void`](#void-bundleputtype-bundle-b-const-char-key-type-v)
    - [`bundle_get_<type> (const bundle *, const char *, <type> *): int`](#int-bundlegettype-const-bundle-b-const-char-key-type-ptr)

## Includes
`#include "collections/bundle.h"`

## Types
### `bundle`
The bundle data type. Always carried around as a pointer, since it is an incomplete type not defined in the header.

### `bundleHashFunc`
A hash function for the elements of a bundle. This is usually defined internally in the bundle, but it may be supplied on creation using the [`bundle_create_hf`](#bundle-bundlecreatehf-bundlehashfunc-hf) function.

<br>

## Methods
### `bundle *bundle_create (void)`
Creates a new bundle.

<br>

### `bundle *bundle_create_hf (bundleHashFunc hf)`
Creates a new bundle with the specified hash function.
<br>
Parameters:
  - [`bundleHashFunc`](#bundlehashfunc) `hf`: The hash function to use for the bundle.

<br>

### `size_t bundle_size (const bundle *b)`
Returns the amount of elements in bundle `b`.
<br>
Parameters:
  - `const` [`bundle`](#bundle) `*b`: The bundle to count the elements from.

<br>

### `size_t bundle_merge (bundle *dst, const bundle *src, size_t *nexist)`
Merges the two bundles into one. Thus `dst` will receive all the keys `src` has that it does not have. If not `NULL`, `nexist` will contain the amount of values transferred from `src` to `dst`. Returns the length of `src`.
<br>
Parameters:
  - [`bundle`](#bundle) `*dst`: The destination bundle to move the values to.
  - `const` [`bundle`](#bundle) `*src`: The source of the moved elements.
  - `size_t *nexist`: The amount of values that was moved.

<br>

### `bool bundle_has_key (const bundle *b, const char *key)`
Returns true if the bundle `b` contains the key `key`.
<br>
Parameters:
  - `const` [`bundle`](#bundle) `*b`: The bundle to check the key from.
  - `const char *key`: The key to check for.

<br>

### `void bundle_destroy (bundle *b)`
Frees the bundle. Note that this method does nothing whatsoever to free the elements.
<br>
Parameters:
  - [`bundle`](#bundle) `b`: The bundle to free.

<br>

### `void bundle_put_<type> (bundle *b, const char *key, <type> v)`
### (see [Supported types](#supported-types))
These methods place the value `v` to the bundle with key `key`. If the key is `NULL` or is already associated with a value, nothing is done.

<br>

### `int bundle_get_<type> (const bundle *b, const char *key, <type> *ptr)`
### (see [Supported types](#supported-types))
These methods place the value associated with the key `key` into `ptr`. If the key is `NULL` or isn't associated with a value, a nonzero value is returned. Otherwise, `ptr` is set to the specified value and 0 is returned.

<br>

<br>

### Content note:
This document was written when commit [c0a6f](https://github.com/apachejuice/cbr/commit/c0a6f9d2e3240806ce35d7ab804a6fa2b7f746c3) was the newest. If this document contains information that does not line up with the current source tree, [create an issue](https://github.com/apachejuice/cbr/issues/new).
