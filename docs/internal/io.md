# I/O

### Table of contents:
  - [Background](#background)
  - [Console](#console)
    - [API index](#api-index)

## Background
I/O APIs in the CBR are available in the `lib/io` directory. They are very much work-in-progress area, so this document is up for change. The APIs described in this document are from commit [11434](https://github.com/apachejuice/cbr/commit/11434f5b066d45cdd497b2b288a0331ce99bf31d).

<br>

# Console
The [`console`](https://github.com/apachejuice/cbr/blob/dev-staging/lib/io/console.h) type acts as an interface to the system command line. It, like the rest of the I/O api is WIP, but it currently can change the text color and print text. Here's an example:
```c
#include "io/console.h"

int main (int argc, char *argv[]) {
    // Get a console object pointing to the standard output
    console c = console_stdout ();
    // Print some text
    console_write (&c, "Hello world!\n");

    // Now change the text color to 0xFF00FF (bright purple)
    console_set_text_color (&c, 0xFF, 0, 0);
    // Print some more text
    console_write (&c, "OwO");

    // Reset the text color
    console_reset_text_color (&c);

    return 0;
}
```

## API index
  - [Includes](#includes)
  - [Types](#types)
    - [`console`](#console-1)
  - [Methods](#methods)
    - [`console_stdout (void): console`](#console-consolestdout-void)
    - [`console_stderr (void): console`](#console-consolestderr-void)
    - [`console_lwrite (console *, const void *, size_t): size_t`](#sizet-consolelwrite-console-con-const-void-buf-sizet-n)
    - [`console_write (console *, const char *): size_t`](#sizet-consolewrite-console-con-const-char-str)
    - [`console_set_text_color (console *, int, int, int): bool`](#bool-consolesettextcolor-console-con-int-red-int-green-int-blue)
    - [`console_reset_text_color (console *): void`](#void-consoleresettextcolor-console-con)
  - [Macros](#macros)
    - [`IS_COLOR(c)`](#iscolorc)

## Includes
`#include "io/console.h"`

## Types
### `console`
The console data type represents a console. It is carried by value, but passed by reference to allow changes to be made to it.

<br>

## Methods
### `console console_stdout (void)`
Returns the console object pointing to the standard output.

<br>

### `console console_stderr (void)`
Returns the console object pointing to the standard error output.

<br>

### `size_t console_lwrite (console *con, const void *buf, size_t n)`
Writes `n` bytes from `buf` to the console `con`. If the write failed, returns 0. Otherwise, the amount of bytes written is returned.
<br>
Parameters:
- [`console`](#console-1) `*con`: The console to write to
- `const void *buf`: The data to write to the console
- `size_t n`: The amount of `buf` to write

<br>

### `size_t console_write (console *con, const char *str)`
Like [`console_lwrite`](#sizet-consolelwrite-console-con-const-void-buf-sizet-n), but takes `str` in place of `buf` and replaces `n` with `strlen (str)`.
<br>
Parameters:
- [`console`](#console-1) `*con`: The console to write to
- `const char *str`: The string to write to the console

<br>

### `bool console_set_text_color (console *con, int red, int green, int blue)`
Sets the console text color into an RGB value consisting of the given colors. Returns true if the color change succeedeed.
<br>
Parameters:
- [`console`](#console-1) `*con`: The console to change the color of
- `int red`: The red color value
- `int green`: The green color value
- `int blue`: The blue color value

<br>

### `void console_reset_text_color (console *con)`
Resets the console text color into its original value
<br>
Parameteres:
- [`console`](#console-1) `*con`: The console to reset

<br>

## Macros
### `IS_COLOR(c)`
### (defined as `(c >= 0 && c <= 255)`)
Checks if the given color value `c` is a valid RGB color component.

<br>

# File
The `file` type is the base interface to the filesystem. On windows, it is a `HANDLE` returned from `CreateFile` and friends, and on Linux it is an `int`.
Here is an example of using the file API:
```c
#include "io/file.h"
#include "system/error.h"
#include "runtime/wtypes.h"

int main (int argc, char *argv[]) {
    // Open the file
    file f;
    int res = file_open_simple (&f, "uwu.txt", FM_READ);

    if (res) {
        printf ("Error opening file: %s\n", error_str (res));
    } else {
        u1 *buf;
        size_t sz;
        file_read_simple (&f, &buf, &sz);
        printf ("File content: %s\n", buf);
        file_close (&f);
    }

    return res ? 1 : 0;
}
```

## API index
- [Includes](#includes-1)
- [Types](#types-1)
  - [`file`](#file-1)
  - [`fileError`](#fileerror)
- [Methods](#methods-2)

## Includes
`#include "io/file.h"`

## Types
### `file`
Represents a file. The value of a file may not be valid, meaning it may not point to an actual file but rather an error value.

### `fileError`
An enumeration representing an error that occurred during the reading of a file.
<br> Values:
  - `F_ERR_SUCCESS`: No error occurred.
  - `F_ERR_NOT_FOUND`: The file could not be found.
  - `F_ERR_NOREAD`: The file could not be read.
  - `F_ERR_2BIG`: The file was too big to be read.
  - `F_ERR_ISDIR`: The file in question is, in fact, a directory.
  - `F_ERR_NOKNOW`: Unknown error.

<br>
