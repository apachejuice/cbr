# CBR Coding Style
This document describes the CBR coding styles such as:
- Formatting
- Naming conventions
- File conventions
- License headers

### Table of contents:
  - [Epilogue](#epilogue)
  - [`clang-format` and other tooling](#clang-format-and-other-tooling)
  - [If `clang-format` does everything for me, why does this document exist?](#if-clang-format-does-everything-for-me-why-does-this-document-exist)
  - [Vertical spacing guide](#vertical-spacing-guide)
  - [Naming conventions](#naming-conventions)

<br>

## Epilogue
Coding style plays an important part in keeping a project readable and clear to outsiders. This document's main objective is to set up guidelines for programming the CBR, which the main part being: if you showed your code to a random C programmer, would they understand what you wrote?

<br>

## `clang-format` and other tooling
Before committing, run the script `format.sh` in the root directory of the repo. If you are on a windows platform, you must specify the `FIND` environment variable to a unix find command to override the standard windows find, like here:
```sh
FIND=/usr/bin/find ./format.sh
```
Do note that you need to be in an msys or equivalent shell to access `/usr/bin/find`.

On platforms with `find` pointing to a unix-compatible `find` command by default, the environment variable is not needed.

The `clang-format` configuration is in the file `.clang-format`. It can be found [here](#the-final-product) (and [here](https://github.com/apachejuice/cbr/blob/dev-staging/.clang-format))
### List of `clang-format` rules used in the project:
  - [`BasedOnStyle: Google`](#basedonstyle-google)
  - [`BreakBeforeBraces: Attach`](#breakbeforebraces-attach)
  - [`AllowShortFunctionsOnASingleLine: false`](#allowshortfunctionsonasingleline-false)
  - [`ColumnLimit: 80`](#columnlimit-80)
  - [`SpaceBeforeParens: Always`](#spacebeforeparens-always)
  - [`PointerAlignment: Right`](#pointeralignment-right)
  - [`ReflowComments: false`](#reflowcomments-false)
  - [`IndentWidth: 4`](#identwidth-4)
  - [`SortIncludes: false`](#sortincludes-false)
  - [`SpaceAfterCStyleCast: true`](#spaceaftercstylecast-true)
  - [`AlignConsecutiveAssignments: true`](#alignconsecutiveassignments-true)
  - [`AllowShortCaseLabelsOnASingleLine: true`](#allowshortcaselabelsonasingleline-true)
  - [`AlignConsecutiveMacros: true`](#alignconsecutivemacros-true)

### `BasedOnStyle: Google`
This line tells `clang-format` to inherit the default values from [Google coding conventions](https://google.github.io/styleguide/). Some of these values will be overridden later.

### `BreakBeforeBraces: Attach`
This prevents `clang-format` from _ever_ placing an opening brace `{` on a newline, like here:
```c
int example_of_bad_formatting()
{
    while(1)
    {
        braces_on_newlines();
    }
}
```
and instead will format like so:
```c
int example_of_good_formatting () {
    while (1) {
        braces_on_preceeding_lines ();
    }
}
```
Placing braces on newlines is bad practice because it only _slightly_ improves readablility with the cost of adding a lot of empty space and increasing line count.

### `AllowShortFunctionsOnASingleLine: false`
By default, `clang-format` will place shorter functions along with their bodies on the same line:
```c
int tiny() { return 1; }
```
with this it will instead format them accordingly:
```c
int tiny() {
    return 1;
}
```
This improves readability a lot.

### `ColumnLimit: 80`
Standard practise in programming, if you want to have something else than the editor open (like a sidebar), long lines are gonna be annoying. This will limit them to 80 columns.

### `SpaceBeforeParens: Always`
This is only a matter of taste. I enjoy having a space before parens in macro/function invocations and control structures such as `while` and `switch`.

### `PointerAlignment: Right`
See this example:
```c
void* i,
int x;
const char* p;
```
Looks fine, right? Now, let's declare a few variables of the same type:
```c
void* i,* p;
int x, y;
```
I think this explains itself. Of course we could do:
```c
void * i, * p;
```
IMHO that still looks slightly strange, plus the spaces around `*` just consume, well, space :D

### `ReflowComments: false`
`clang-format` tends to reflow comments... strangely. Let see this example:
```c
/* This is a really long documentation comment that definetly does take over 80 columns. See that 0 there? That was at the 80 col mark. */
int x;
```
Formatting this with `ReflowComments: true` gives
```c
/* This is a really long documentation comment that definetly does take over 80
 * columns. See that 0 there? That was at the 80 col mark. */
int x;
```
- I personally don't like the `*` at the beginning
- The comment does not space out evenly; the lines end at a hugely different column mark
<br>A properly formatted comment is as follows:

```c
/* This is a really long documentation comment that definetly does
take over 80 columns. See that 0 there? That was at the 80 col mark. */
int x;
```

### `IdentWidth: 4`
4 is the industry-standard ident width. 2 tends to make them unclear and 8 approaches the 80-column mark fast.

### `SortIncludes: false`
What are we, _nuclear physicists_? Include sorting seems to space them out too far apart too.

### `SpaceAfterCStyleCast: true`
By default, C style casts (they're called c-style because clang-format supports C++ too, but it refers to `(int) x`) are formatted with the ending parentheses combined to the following expression, like so:
```c
(void *)value;
```
However,
```c
(void *) value;
```
is more readable.

### `AlignConsecutiveAssignments: true`
First we should define what "consecutive" means to `clang-format`. Two units of source code are consecutive if:
- They appear in the same scope
- They are not separated by any newlines
- They are not separated by any comments

As such, the following statements are all consecutive
```c
int x = 3;
double y = 23.23;
struct coord xy = { (double) x, y };
```
and as such, `clang-format` will turn them into
```c
int x           = 3;
double y        = 23.23;
struct coord xy = { (double) x, y };
```
This _is_ slightly PITA with long typenames:
```c
somereallylongstructnamewhywouldanyonemakethis  s = { 1, 2, NULL };
int                                             x = 3;
double                                          y = 23.23;
```
but IMHO it is still better than having a mess of assignments.

### `AllowShortCaseLabelsOnASingleLine: true`
If we have a switch statement, there is no point in this kind of formatting:
```c
int value = get_value ();
switch (value) {
    case VAL_A:
        return "uwu";
    case VAL_B:
        return "owo";
    case VAL_C:
        return "^.^";
    // ..and so on
}
```
Instead, the case labels can be compactly put on a single line:
```c
int value = get_value ();
switch (value) {
    case VAL_A: return "uwu";
    case VAL_B: return "owo";
    case VAL_C: return "^.^";
    // ..and so on
}
```
In addition, `clang-format` will align the label blocks nicely.

### `AlignConsecutiveMacros: true`
Same as [`AlignConsecutiveAssignments`](#alignconsecutiveassignments-true) but for macros:
```c
#define FOO         1
#define BARBARBAR   2
#define CUTIE_UWU   4
```

### The final product
This is the CBR's clang-format:
```yaml
BasedOnStyle: Google
BreakBeforeBraces: Attach
AllowShortFunctionsOnASingleLine: false
ColumnLimit: 80
SpaceBeforeParens: Always
PointerAlignment: Right
ReflowComments: false
IndentWidth: 4
SortIncludes: false
SpaceAfterCStyleCast: true
AlignConsecutiveAssignments: true
AllowShortCaseLabelsOnASingleLine: true
AlignConsecutiveMacros: true
```

## If `clang-format` does everything for me, why does this document exist?
- To remind you to actually run `./format.sh`
- No, it does not do everything for you!

`clang-format` cannot control your [_vertical spacing_](#vertical-spacing-guide) (the amount of newlines between things), in particular inside block bodies (it does format outside of them though).

`clang-format` cannot control your [naming conventions](#naming-conventions) either.

## Vertical spacing guide
- [In general](#in-general)
- [In consecutive variable declarations](#in-consecutive-variable-declarations)
- [In the top-level scope](#in-the-top-level-scope)
- [In long macro definitions](#in-long-macro-definitions)

### In general
- Avoid more than 2 empty lines in a row
- If your function/macro is more than 5 lines, consider adding spaces to it
- Empty newline at the end of a file (can be configured for most editors)

### In consecutive variable declarations
Prefer having initialization before a block statement separated from the block statement by a newline _if_ there is more than one line of initialization. So don't do this:
```c
int x = get_x ();
int y = get_y ();
double z = 2.31;
if (somefunc (x, y, z)) {
    // ...
}
```
instead do
```c
int x = get_x ();
int y = get_y ();
double z = 2.31;

if (somefunc (x, y, z)) {
    // ...
}
```
However
```c
double z = 2.31;
if (someotherfunc (z)) {
    // ...
}
```
is fine.

Prefer grouping declarations related to the same thing in a group, separated from other declarations. Consider the following function:
```c
struct something get_something (int someparam, double someother) {
    double rounded = round_prec (2, someother);
    const char *serialized = serialize (rounded, someparam);
    struct information data = make_data (serialized);
    component c1 = get_c1 (someparam, data);

    // ... more processing
    component c2 = get_c2 ();

    return new_something (c1, c2);
}
```
It would simply not make sense to group the initialization of `c1` and `c2` together. It would make the source less readable too. This especially applies in function bodies, but also global variables.

### In the top-level scope
`clang-format` does a decent job here. Keep function _definitions_ a newline apart. However, if you have prototypes, group them by category. And please add _some_ documentation comments to prototypes describing their purpose.

### In long macro definitions
If you have a long-ish macro, `clang-format` will format it with the same rules as any other C declaration. Sometimes that looks weird, so if you don't like it, surround the macro with special comments like so:
```c
// clang-format off
#define annoyingly_long_macro(b) { if (b) { printf ("Long macro here!\n"); } }
// clang-format on
```
and `clang-format` will leave your macro alone :)


## Naming conventions
Ah yes, naming conventions. The most controversial part of programming.
### Table of contents:
  - [Types](#types)
  - [Methods](#methods)
  - [Local variables](#local-variables)

### Types
If the type only consists of one word, like `bundle`, `file` or `console`, just use that word in all lowercase. Otherwise, you may use `camelCase`. Think `vmThread` or `codeGenerator`.

### Methods
If your type is `fooBar` and you want to have a "member" method `print`, name it `foo_bar_print`. Methods names shall be in all lowercase with underscores as word separators.

### Local variables
No hungarian notation, just descriptive names with the same rules as for [methods](#methods).
