#!/usr/bin/env bash
CFMT=clang-format
command -v $CFMT 1>/dev/null 2>&1 || { echo "Cannot find clang-format; exiting."; exit 1; }
if [ -z "$1" ]; then
    FIND=find
else
    FIND="$1"
fi

FILES=$("$FIND" lib/ -type f \( -name '*.c' -o -name '*.h' \))
COUNT=$(echo $FILES | wc -w)
TOTALLINES=0
UP=0
DOWN=0
LEVEL=0
echo "Formatting $COUNT files."

for file in $FILES; do
    COUNTER=$((COUNTER + 1))
    printf "Formatting file %s (%d / %d)... " "$file" "$COUNTER" "$COUNT"
    OLDLINES=$(wc -l < "$file")
    $CFMT -i "$file"
    LINES=$(wc -l < "$file")
    TOTALLINES=$((TOTALLINES + LINES))
    DIFF=$((LINES - OLDLINES))
    echo "$DIFF lines."

    if [ $DIFF -eq 0 ]; then
        LEVEL=$((LEVEL + 1))
    elif [ $DIFF -gt 0 ]; then
        UP=$((UP + 1))
    else
        DOWN=$((DOWN + 1))
    fi
done

echo "Formatted $COUNT files, $TOTALLINES lines of output."
echo "$UP increased in lines, $DOWN decreased in lines, $LEVEL unchanged."

