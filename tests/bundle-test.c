/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <check.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "collections/bundle.h"
#include "perhaps.h"

START_TEST (bundle_values_test) {
    bundle *b = bundle_create ();
    bundle_put_int (b, "foo", 2);
    bundle_put_double (b, "foo", 2.2);

    // Make sure values persist
    int val;
    bundle_get_int (b, "foo", &val);
    ck_assert_int_eq (val, 2);

    // Make sure incorrectly typed queries fail
    double val2 = NAN;
    bundle_get_double (b, "foo", &val2);
    ck_assert (isnan (val2));
}

END_TEST

START_TEST (bundle_merge_test) {
    // Make sure merging works
    bundle *b1 = bundle_create ();
    bundle_put_double (b1, "d1", 0.1);
    bundle_put_int (b1, "i1", 123);

    bundle *b2 = bundle_create ();
    bundle_put_double (b2, "d2", 9.2);
    bundle_put_int (b2, "i2", 234);

    size_t nex;
    bundle_merge (b2, b1, &nex);

    double d1 = 0.0, d2 = 0.0;
    int i1 = 0, i2 = 0;
    bundle_get_int (b2, "i1", &i1);
    bundle_get_int (b2, "i2", &i2);
    bundle_get_double (b2, "d1", &d1);
    bundle_get_double (b2, "d2", &d2);

    ck_assert_double_eq (d1, 0.1);
    ck_assert_double_eq (d2, 9.2);
    ck_assert_int_eq (i1, 123);
    ck_assert_int_eq (i2, 234);
}

END_TEST

Suite *create_suite () {
    Suite *s;
    TCase *values, *merge;

    s      = suite_create ("bundle");
    values = tcase_create ("values");
    merge  = tcase_create ("merge");
    tcase_add_test (values, bundle_values_test);
    tcase_add_test (merge, bundle_merge_test);
    suite_add_tcase (s, values);
    suite_add_tcase (s, merge);

    return s;
}

int main (int argc, char *argv[]) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s  = create_suite ();
    sr = srunner_create (s);

    srunner_run_all (sr, CK_VERBOSE);
    number_failed = srunner_ntests_failed (sr);
    srunner_free (sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
