/*
 * Copyright 2021-2022 apachejuice
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <check.h>
#include <stdlib.h>
#include "bytecode/attrmap.h"

#define ATTR     "Hellu-Attr"
#define ATTRLEN  sizeof (ATTR)
#define ATTR2    "Another-Attr"
#define ATTR2LEN sizeof (ATTR2)

START_TEST (attr_map_values) {
    attrmap map;
    attrmap_new (&map, 2);

    char value1data[] = "uwuoow";
    attrmap_push (&map, ATTR, ATTRLEN, value1data, sizeof (value1data), NULL);
    ck_assert_str_eq (value1data, attrmap_get (&map, ATTR, ATTRLEN, NULL));

    char value2data[] = "uwu-oow";
    attrmap_push (&map, ATTR2, ATTR2LEN, value2data, sizeof (value2data), NULL);
    ck_assert_str_eq (value2data, attrmap_get (&map, ATTR2, ATTR2LEN, NULL));
}
END_TEST

Suite *create_suite () {
    Suite *s;
    TCase *values;

    s      = suite_create ("attrmap");
    values = tcase_create ("values");
    tcase_add_test (values, attr_map_values);
    suite_add_tcase (s, values);

    return s;
}

int main (int argc, char *argv[]) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s  = create_suite ();
    sr = srunner_create (s);

    srunner_run_all (sr, CK_VERBOSE);
    number_failed = srunner_ntests_failed (sr);
    srunner_free (sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
